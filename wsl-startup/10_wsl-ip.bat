@echo off

set WSL_IP_PREFIX=192.168.123
set WSL_IP=%WSL_IP_PREFIX%.100
set WSL_IF_IP=%WSL_IP_PREFIX%.1

REM --add the following to the top of your bat file--
@echo off
:: BatchGotAdmin
:-------------------------------------
REM --> Check for permissions

>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.

if '%errorlevel%' NEQ '0' (
 echo Requesting administrative privileges...
 goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
 echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
 set params = %*:"=""
 echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

 "%temp%\getadmin.vbs"
 del "%temp%\getadmin.vbs"
 exit /B

:gotAdmin
 pushd "%CD%"
 CD /D "%~dp0"
:--------------------------------------

rem C:\Windows\System32\bash.exe -c "sudo service smbd start"
wsl -d Ubuntu -u root ip addr add %WSL_IP%/24 broadcast %WSL_IP_PREFIX%.255 dev eth0 label eth0:1
netsh interface ip add address "vEthernet (WSL)" %WSL_IF_IP% 255.255.255.0

:---- WSL Firewall Unlock ----
PowerShell.exe -ExecutionPolicy Bypass -File .\11_wsl-firewall-unlock.ps1

wsl -d ubuntu -u root service ssh start
