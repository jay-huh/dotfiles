# How to SSH into Bash and WSL2 on Windows From an External Machine

https://bonguides.com/how-to-ssh-into-bash-and-wsl2-on-windows-from-an-external-machine/

## Forward port from Windows into Linux WSL distro

1. Run the following command in a PowerShell window

```
netsh interface portproxy add v4tov4 `
        listenport=22 `
        listenaddress=0.0.0.0 `
        connectport=22 `
        connectaddress=172.31.138.23
```

2. Check the Port Forwarding

```
netsh interface portproxy show all
```

3. Add a new firewall rule to enable the port on the host machine:

```
netsh advfirewall firewall add rule `
  name="Open Port 22 - WSL 2" dir=in action=allow protocol=TCP localport=22

```
