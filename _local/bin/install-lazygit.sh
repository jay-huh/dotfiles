#!/bin/bash

# INFO: Install lazygit on Ubuntu 20.04

INSTALL_PATH=~/.local/bin

if [[ $OSTYPE == "darwin"* ]] ; then
  exit 1
fi

LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"

sudo tar xf lazygit.tar.gz -C ${INSTALL_PATH} lazygit

lazygit --version

rm -rf lazygit.tar.gz
