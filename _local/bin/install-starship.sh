#!/bin/bash

# INFO: Install starship on Ubuntu 20.04

if [[ $OSTYPE == "darwin"* ]] ; then
  exit 1
fi

curl -sS https://starship.rs/install.sh | sh
