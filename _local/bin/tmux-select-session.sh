#!/usr/bin/env bash

export TERM=xterm-256color

# fzf: `-1` option(do not start interactive finder and automatically select the only match)
SESSION_NAME=$(tmux list-sessions | awk '{print $1}'| tr : ' '|fzf -1)

if [[ -z $TMUX ]] ; then
  tmux attach -t ${SESSION_NAME}
else
  tmux switch-client -t ${SESSION_NAME}
fi
