#!/bin/sh

echo -n "[ GIT ] Enter Username: "
read name
echo -n "[ GIT ] Enter email: "
read email

git config --global user.name "$name"
git config --global user.email "$email"

git config --global core.editor vi
git config --global core.autocrlf input
git config --global core.eol lf
git config --global core.filemode true

git config --global pull.rebase true
git config --global remote.origin.prune true

git config --global merge.tool vimdiff
git config --global merge.conflictstyle diff3
git config --global merge.prompt false

git config --global diff.compactionHeuristic true

git config --global core.excludesfile ~/.gitignore

git config --global pager.color true
git config --global commit.verbose true
git config --global commit.gpgsign true

#git config --global core.pager 'less -F -X'
# diff-highlight
#git config --global pager.log 'diff-highlight | less -F -X'
#git config --global pager.show 'diff-highlight | less -F -X'
#git config --global pager.diff 'diff-highlight | less -F -X'
#git config --global interactive.diffFilter diff-highlight

git config --global alias.lg "log --graph --pretty=format:'%C(auto)%h -%d %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
git config --global --replace-all alias.ch "\!git checkout \$(git branch -vv | fzf --no-preview | awk '{print \$1}')"

echo -e "[ GPG ] Register GPG key."
echo -e "git config --global commit.gpgsign true"
echo -e "git config --global user.signingkey [keyid]"

# delta config
git config --global core.pager delta

git config --global delta.line-numbers true
# in command line: DELTA_FEATURES=side-by-side git show
git config --global delta.side-by-side false
git config --global delta.features 'decorations'

git config --global delta.decorations.commit-decoration-style "blue ol"
git config --global delta.decorations.commit-style raw
git config --global delta.decorations.file-style omit

#git config --global delta.minus-style 'syntax auto'
#git config --global delta.line-numbers-minus-style 'black'
#git config --global delta.minus-emph-style 'red auto'
#
#git config --global delta.plus-style 'syntax omit'
#git config --global delta.line-numbers-plus-style 'green'
#git config --global delta.plus-emph-style 'green auto'
#
#git config --global delta.syntax-theme 'black'

git config --global interactive.diffFilter 'delta --color-only --feature=interactive'

# Register to gitconfig
#git config --global user.signingkey 7DADA874

## Generate a GPG Key pair
## gpg version > 2.1.17
#gpg --full-generate-key
## gpg version < 2.1.17
#gpg --default-new-key-algo rsa4096 --gen-key

#gpg --delete-secret-key [id]
#gpg --delete-key [id]

#gpg --list-keys --keyid-format SHORT

## Import Public key
#gpg --import [filename.pub]
#gpg --import [filename.secret]

## Export Secret key
#gpg --armor --output [filename] --export [keyid]
#gpg --armor --output [filename] --export-secret-keys [keyid]
