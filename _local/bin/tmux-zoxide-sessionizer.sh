#!/usr/bin/env bash

export TERM=xterm-256color

DIRECTORY=$HOME

if [[ ! -z $1 ]] ; then
  #ZOXIDE_RESULT=$(zoxide query $1)
  echo "Select directory in zoxide database..."
  # fzf: `-1` option(do not start interactive finder and automatically select the only match)
  ZOXIDE_RESULT=$(zoxide query -ls | grep $1 | awk '{print $2}' | fzf)
fi

if [[ -d "${ZOXIDE_RESULT}" ]] ; then
  EXPECTED_DIRECTORY=${ZOXIDE_RESULT}
else
  echo "Select directory in $HOME..."
  QUERY=$1
  EXPECTED_DIRECTORY=$(find ${DIRECTORY} -mindepth 1 -maxdepth 4 \
    ! \( -name "Documents" -prune \) ! \( -name "Downloads" -prune \) \
    ! \( -name "Music" -prune \) ! \( -name "Movies" -prune \) \
    ! \( -name "Pictures" -prune \) ! \( -name "Desktop" -prune \) \
    ! \( -name "Library" -prune \) ! \( -name "Applications" -prune \) \
    ! \( -name ".*" -prune \) -type d -print | sort | fzf --query="${QUERY}")
fi

if [[ -z ${EXPECTED_DIRECTORY} ]] ; then
  echo "Directory NOT selected!!!!"
  exit 0;
fi

# if directory not found in zoxide database, add the directory into database
if [[ -z "${ZOXIDE_RESULT}" ]] ; then
  ZOXIDE_RESULT=$(zoxide add ${EXPECTED_DIRECTORY})
fi

SESSION_NAME=$(basename "${EXPECTED_DIRECTORY}" | tr . _)
TMUX_RUNNING=$(pgrep tmux)

if [[ -z $TMUX ]] && [[ -z ${TMUX_RUNNING} ]]; then
  tmux new-session -s ${SESSION_NAME} -c ${EXPECTED_DIRECTORY}
  exit 0
fi

if ! tmux has-session -t ${SESSION_NAME} 2> /dev/null; then
  tmux new-session -ds ${SESSION_NAME} -c ${EXPECTED_DIRECTORY}
fi

if [[ -z $TMUX ]] ; then
  tmux attach -t ${SESSION_NAME}
else
  tmux switch-client -t ${SESSION_NAME}
fi
