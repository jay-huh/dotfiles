#!/bin/sh

CROSS_TOOL_PATH="$HOME/.espressif/tools/xtensa-esp32-elf/esp-2020r3-8.4.0/xtensa-esp32-elf/bin"
CROSS_TOOL_PREFIX="xtensa-esp32-elf-"
ADDR2LINE="${CROSS_TOOL_PATH}/${CROSS_TOOL_PREFIX}addr2line"
ADDR2LINE_OPTIONS="-pfiC -a"

# define strings to find crash point in the log file
GREP_STR_OPTIONS=("-e 'Guru Meditation' -A 12"
"-e 'CORRUPT HEAP' -A 6")
STRINGS=("Guru Meditation Error:"
"CORRUPT HEAP")
LINES=("15"
"6")

verify_options() {
  if [[ ! -e $LOG_FILE ]] ; then
    echo "log file $LOG_FILE not found."
    usage
    exit -1
  fi
  if [[ ! -z "$ELF_FILE" ]] ; then
    if [[ ! -e $ELF_FILE ]] ; then
      echo "elf file $ELF_FILE not found."
      usage
      exit -1
    fi
  fi
}

prepare() {
  # separate name and extension
  LOG_BASENAME=${LOG_FILE_BASE_NAME%.*}
  LOG_EXTENSION=${LOG_FILE_BASE_NAME##*.}

  CRASH_FILE=${LOG_BASENAME}-crash.${LOG_EXTENSION}
  CRASH_BACKTRACE_FILE=${LOG_BASENAME}-crash-backtrace.${LOG_EXTENSION}
  BACKTRACE_FILE=${LOG_BASENAME}-backtrace.${LOG_EXTENSION}
  BACKTRACE_RESULT_FILE=${LOG_BASENAME}-backtrace-result.${LOG_EXTENSION}

  if [[ -e $CRASH_FILE ]] ; then
    echo "Remove pre-generated files..."
    rm -f $CRASH_FILE $CRASH_BACKTRACE_FILE $BACKTRACE_FILE $BACKTRACE_RESULT_FILE
  fi
}

generate_crash_file() {
  echo "\nGenerate Crash File: $CRASH_FILE from $LOG_FILE\n"
  for ((i = 0; i < ${#GREP_STR_OPTIONS[@]}; i++))
  do
    # Generate temporary crash log file
    grep -i "${STRINGS[$i]}" -A ${LINES[$i]} $LOG_FILE >> ${CRASH_FILE}_$i
    COUNT=$(grep -c -i "${STRINGS[$i]}" ${CRASH_FILE}_$i)
    echo ">>> ${STRINGS[$i]} Count: $COUNT"

    # Append backtrace file from temporary crash log
    grep -i -e "${STRINGS[$i]}" -e "backtrace:" ${CRASH_FILE}_$i >> $CRASH_BACKTRACE_FILE

    # Append temporary crash file to main crash log file
    cat ${CRASH_FILE}_$i >> $CRASH_FILE
    rm ${CRASH_FILE}_$i
  done
}

generate_backtrace_file() {
  echo "\nGenerate Backtrace File: $BACKTRACE_FILE from $LOG_FILE\n"
  grep -i "backtrace:" $LOG_FILE > $BACKTRACE_FILE
  COUNT=$(grep -c -i "backtrace:" $BACKTRACE_FILE)
  echo ">>> Backtrace Count: $COUNT"
}

backtrace() {
  echo "\nStart backtracing from $BACKTRACE_FILE using $ELF_FILE"
  if [[ ! -z "$ELF_FILE" ]] ; then
    # delete any characters trailing with 'Backtrace: '
    sed 's/.*Backtrace: //g' $BACKTRACE_FILE > addr.txt

    while IFS= read -r ADDRESS
    do
      echo "\n>>> Backtrace address: $ADDRESS" >> $BACKTRACE_RESULT_FILE
      echo "Commnad: $ADDR2LINE $ADDR2LINE_OPTIONS $ADDRESS\n" >> $BACKTRACE_RESULT_FILE
      $ADDR2LINE $ADDR2LINE_OPTIONS $ADDRESS -e $ELF_FILE >> $BACKTRACE_RESULT_FILE
    done < addr.txt
    rm addr.txt
  fi

  if [[ ! -z $SHOW_RESULT ]] ; then
    cat $BACKTRACE_RESULT_FILE
  fi
}

usage() {
  echo "Usage : $0
  -l : console log file
  -e : elf file for backtrace
  -v : show backtrace result
  -h : show this messages"
}

# Parse options
OPTSPEC="l:e:vh"
while getopts $OPTSPEC opt ; do
  case "$opt" in
    l)
      LOG_FILE_BASE_NAME=$(basename ${OPTARG})
      LOG_FILE=${OPTARG}
      ;;
    e)
      ELF_FILE=${OPTARG}
      ;;
    v)
      SHOW_RESULT=1
      ;;
    h)
      usage
      exit 0
      ;;
    \?)
      #echo "Invalid option '$opt'"
      usage
      exit -1
      ;;
  esac
done

# main
verify_options
prepare
generate_crash_file
generate_backtrace_file
backtrace
