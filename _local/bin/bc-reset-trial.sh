#!/bin/sh

if [[ ${OSTYPE} == "darwin"* ]] ; then
  REGISTRY_FILE="${HOME}/Library/Application Support/Beyond Compare 5/registry.dat"
else
  # TODO: NOT tested...
  REGISTRY_FILE="${HOME}/.config/bcompare/registry.dat"
fi

echo "REMOVE: ${REGISTRY_FILE}"
rm -v "${REGISTRY_FILE}"
