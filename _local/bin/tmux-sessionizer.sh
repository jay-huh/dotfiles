#!/usr/bin/env bash

export TERM=xterm-256color

DIRECTORY=$HOME

if [[ -d $1 ]] ; then
  DIRECTORY=$1
else
  QUERY=$1
fi

EXPECTED_DIRECTORY=$(find ${DIRECTORY} -mindepth 1 -maxdepth 4 \
  ! \( -name "Documents" -prune \) ! \( -name "Downloads" -prune \) \
  ! \( -name "Music" -prune \) ! \( -name "Movies" -prune \) \
  ! \( -name "Pictures" -prune \) ! \( -name "Desktop" -prune \) \
  ! \( -name "Library" -prune \) ! \( -name "Applications" -prune \) \
  ! \( -name ".*" -prune \) -type d -print | sort | fzf --query="${QUERY}")

if [[ -z ${EXPECTED_DIRECTORY} ]] ; then
  echo "Directory NOT selected!!!!"
  exit 0;
fi

SESSION_NAME=$(basename "${EXPECTED_DIRECTORY}" | tr . _)
TMUX_RUNNING=$(pgrep tmux)

if [[ -z $TMUX ]] && [[ -z ${TMUX_RUNNING} ]]; then
  tmux new-session -s ${SESSION_NAME} -c ${EXPECTED_DIRECTORY}
  exit 0
fi

if ! tmux has-session -t ${SESSION_NAME} 2> /dev/null; then
  tmux new-session -ds ${SESSION_NAME} -c ${EXPECTED_DIRECTORY}
fi

if [[ -z $TMUX ]] ; then
  tmux attach -t ${SESSION_NAME}
else
  tmux switch-client -t ${SESSION_NAME}
fi
