#!/bin/bash

COLOR_SH="$HOME/.local/bin/color.sh"

KERNEL_HEADER_DIR=/usr/src/linux-headers-`uname -r`
KERNEL_SOURCE_DIR=/usr/src/linux-source-`uname -r|sed "s/-[0-9]*[a-z]*//g"`
LINUX_INCLUDE_DIR=/usr/include
PYTHON_DIRS=$(python -c "import os, sys; print(' '.join('{}'.format(d) for d in sys.path if os.path.isdir(d)))")
EXEC_DIR=`pwd`

# langmap: move into ~/.ctags
#CTAGS_OPT="--langmap=sh:+.inc,asm:+.lds,asm:+.scl,make:+.min"
CTAGS_OPT=$CTAGS_OPT" --links=no"
#CTAGS_OPT=$CTAGS_OPT" --c++-kinds=+p --fields=+iaS --extra=+q"
CTAGS_OPT=$CTAGS_OPT" --python-kinds=-i --fields=+iaSl --extra=+q"
CTAGS_OPT=$CTAGS_OPT" --exclude=CVS \
  --exclude=.svn \
  --exclude=.git \
  --exclude=*.js \
  --exclude=*.o \
  --exclude=*.d \
  --exclude=*.bak"
  CTAGS_OPT=$CTAGS_OPT" -R"

  if [ -e $COLOR_SH ] ; then
    source $COLOR_SH
    #echo -e $RED_BOLD"Including Color.sh"$ENDCOLOR
  fi

#for arg in $*
#do
#	echo $arg
#done

#for index in $(seq 1 1 $#)
#do
#	echo $index: $1
#	shift
#done

#if [ -z $1 ] || [ -z $2 ] || [ "$1" = "-h" ] ; then
if [ -z $1 ] || [ "$1" = "-h" ] ; then
  echo "## Usage:  $(basename $0) [ -c | -C | -r | -h ] path "
  echo "           for other directory: $(basename $0) -c <path>"
  echo "           -c : Create tags file ex) $(basename $0) -c ./ or $(basename $0) -c <path>"
  echo "           -r : Remove tags file"
  echo "           -h : Display this messages"
  exit 0;
fi

for dir in $EXCLUDE_DIR
do
  CTAGS_OPT=$CTAGS_OPT" --exclude=$dir"
done

if [ "$1" = "-r" ] ; then
  echo -e $RED_BOLD"Removing CTAGS in "$BLUE_BOLD"`pwd`"$ENDCOLOR
  rm -f ./tags
  rm -f ./cscope.*
  shift
  for i in $(seq 1 1 $#)
  do
    if [ -e $1 ] ; then
      echo -e $RED_BOLD"Removing CTAGS in "$BLUE_BOLD"$1\n"$ENDCOLOR
      rm -f $1/tags
      rm -f $1/cscope.*
    else
      echo -e $RED"Warning: "$ENDCOLOR"Path [ $1 ] is not found."
    fi
    shift
  done
  exit 0
elif [ "$1"  = "-c" ] || [ "$1" = "-C" ] ; then
  echo -e $CYAN_BOLD"CTAGS options: \n"$ENDCOLOR"$CTAGS_OPT\n"
  echo -e $BLUE_BOLD"EXCLUDE_DIR:"$YELLOW" $EXCLUDE_DIR\n"$ENDCOLOR
  echo -e $BLUE_BOLD"Python directory: \n"$ENDCOLOR"$PYTHON_DIRS\n"
  echo -e $BLUE_BOLD"Current directory: \n"$ENDCOLOR"`pwd`"

  cd $EXEC_DIR
  echo -e "\nSTART Creating CTAGS in "$RED_BOLD"`pwd`"$ENDCOLOR
  shift

  for i in $(seq 1 1 $#)
  do
    if [ -e $1 ] ; then
      #cd $1
      echo -e "Appending directory "$BLUE_BOLD"$1"$ENDCOLOR
      DIR=$1
      APPEND_DIRS="$APPEND_DIRS $DIR"
    else
      echo -e $RED"Warning: "$ENDCOLOR"Path [ $1 ] is not found."
    fi
    shift
  done
  APPEND_DIRS="$APPEND_DIRS $PYTHON_DIRS"

  echo "APPEND: $APPEND_DIRS"
  echo -e $RED_BOLD"Removing CTAGS in "$BLUE_BOLD"`pwd`"$ENDCOLOR
  rm -f ./tags
  rm -f ./cscope.*

  #for i in $(seq 1 1 $#)
  for DIR in $APPEND_DIRS
  do
    if [ -d $DIR ] ; then
      echo -e "Creating CTAGS in "$BLUE_BOLD"$DIR"$ENDCOLOR
      ctags $CTAGS_OPT --append=yes $DIR
      echo "ctags $CTAGS_OPT --append=yes $DIR"
    else
      echo -e $RED"Warning: "$ENDCOLOR"Path [ $1 ] is not found."
    fi
  done
  echo -e "\nCreating CTAGS Completed in "$RED_BOLD"`pwd`"$ENDCOLOR
fi

# cscope
EXCLUDE_DIR=".svn .git "$EXCLUDE_DIR
CSCOPE_OPTION=""

for dir in $EXCLUDE_DIR
do
	#CSCOPE_EXCLUDE_OPT="! ( -type d -name "$dir" -prune ) "$CSCOPE_EXCLUDE_OPT
	CSCOPE_EXCLUDE_OPT="! ( -path "$dir" -prune ) "$CSCOPE_EXCLUDE_OPT
done

find $APPEND_DIRS $CSCOPE_EXCLUDE_OPT \( \
  -name '*.py' -o -name '*.py[xdi]' -o -name '*.scons' -o \
  -name '*.[cCsShHx]' -o \
  -name '*.cpp' -o -name '*.cc' -o -name '*.hh' -o -name '*.hpp' -o \
  -name '*.ld' -o -name '*.lds' -o -name '*.scl' -o \
  -name '*.inc' -o \
  -name '[mM]akefile' -o -name '*.mk' -o -name '*.min' -o \
  -name '*.java' -o -name '*.aidl' -o -name '*.xml' \
  \) -type f > cscope.files

  cscope -b $CSCOPE_OPTION -i cscope.files
