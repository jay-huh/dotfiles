#!/bin/sh

# install nodejs
sudo apt install -y nodejs

# install yarn
npm install -g yarn

cd ${HOME}/.vim/plugged/coc.nvim
yarn install

# In VIM
# :CocInstall coc-clangd
# :CocCommand cland.install

stow -v -R -t ${HOME}/.vim --no-folding _vim
