#!/bin/sh

stow -v -R -t ${HOME} --no-folding vim
stow -v -R -t ${HOME} home-dir

#stow -v -R -t ${HOME}/.config --no-folding _config
stow -v -R -t ${HOME}/.config _config

stow -v -R -t ${HOME} --no-folding bash
stow -v -D -t ${HOME} bash

stow -v -R -t ${HOME} --no-folding zsh

stow -v -R -t ${HOME}/.vim --no-folding _vim
