# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

echo "START .bashrc"

# append to the history file, don't overwrite it
shopt -s histappend
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

source ${HOME}/.commonrc.sh

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

#	Hangul input( C-space ) for Xming
FCITX_ENABLE=`ps -aux | grep fcitx | grep -v grep | wc -l`
if [ "$FCITX_ENABLE" == "0" ] ; then
	#export XMODIFIERS=@im=fcitx
	#fcitx 2> /dev/null		#	disable STDERR Output
	echo "Start FCITX."
else
	echo "FCITX is Running."
fi
unset FCITX_ENABLE

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

PS1="${debian_chroot:+($debian_chroot)}\[\033[01;33m\]\u\[\033[00m\]@\[\033[00;32m\]\h\[\033[00m\]:\[\033[00;36m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\]\n$ "

ENABLE_POWERLINE=0
if [ "$TTY_TYPE" == "tty" ] || [ ! -z "$IS_DOCKER" ] ; then
	ENABLE_POWERLINE=0
fi

#if [ -f "`which powerline-daemon`" ] ; then
if [ "$ENABLE_POWERLINE" != "0" ] ; then
	echo "RUN powerline"
	powerline-daemon -q
	POWERLINE_BASH_CONTINUATION=1
	POWERLINE_BASH_SELECT=1
	. /usr/share/powerline/bindings/bash/powerline.sh
	#. /usr/local/lib/python2.7/dist-packages/powerline/bindings/bash/powerline.sh
	#. /usr/share/powerline/bindings/shell/powerline.sh
fi

set -o vi

# 31 – red
# 32 – green
# 33 – yellow
# 0 – reset/normal
# 1 – bold
# 4 – underlined
LESS_COLOR=2
if [[ ! -z ${LESS_COLOR} ]] ; then
	export LESS_TERMCAP_mb=$'\e[1;32m'
	export LESS_TERMCAP_md=$'\e[1;32m'
	export LESS_TERMCAP_me=$'\e[0m'
	export LESS_TERMCAP_se=$'\e[0m'
	export LESS_TERMCAP_so=$'\e[01;33m'
	export LESS_TERMCAP_ue=$'\e[0m'
	export LESS_TERMCAP_us=$'\e[1;4;31m'
fi
unset LESS_COLOR

#if [[ ! -z $(which most) ]] ; then
#	export PAGER=most
#fi

echo "END .bashrc"

# mount overlayfs excample
#sudo mount -t overlay overlay -o lowerdir=./lower,upperdir=./out/upper,workdir=./out/work ./out/merged

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

eval "$(starship init bash)"
