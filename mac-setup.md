
## Install python and pip
```
# python and pip installed in '/opt/homebrew/bin/'
brew install python3
brew install brew-pip

% python3 --version
Python 3.9.10

% pip3 --version
pip 22.0.3 from /opt/homebrew/lib/python3.9/site-packages/pip (python 3.9)

# Make symbolic link for default python as version 3
ln -sfn /opt/homebrew/bin/python3 /opt/homebrew/bin/python
ln -sf /opt/homebrew/bin/pip3 /opt/homebrew/bin/pip

# Set `PATH` so that `/opt/homebrew/bin` has a higher `PATH` priority than `/usr/bin`.
# add follow line into `~/.zshrc`
PATH="/opt/homebrew/bin:$PATH"
```

## Check what Python version you have:

```
# Apply new `PATH` environment variable
source ~/.zshrc

% which python
/opt/homebrew/bin/python

% which pip3
/opt/homebrew/bin/pip3

% python --version
Python 3.9.10

% pip --version
pip 22.0.3 from /opt/homebrew/lib/python3.9/site-packages/pip (python 3.9)
```

## Install host tools for development

### vscode, vim

such as build-essential, clang-format, clang, clangd, gdb-multiarch


```
brew install coreutils
brew install ripgrep colordiff

# code editors: vim or macvim and vscode
brew install vim
brew install --cask visual-studio-code

# merge tool and serial console monitoring tool
brew install --cask meld
brew install --cask coolterm

# llvm includes clang tools
brew install llvm
brew install cppcheck

```

### Install ARM Toolchain


- arm-none-eabi: stm32 and cc1352

- GitHub - xpack-dev-tools/arm-none-eabi-gcc-xpack: A binary distribution of the Arm Embedded GCC toolchain


```
# arm-none-eabi toolchain for arm64 native
brew install npm
npm install --global xpm@latest

xpm install --global @xpack-dev-tools/arm-none-eabi-gcc@latest --verbose

```

### ESP32

```
# install CMake & Ninja build
brew install cmake ninja dfu-util

```

### Install ESP-IDF

- Install ESP-IDF tools: toolchain, python environment, openocd…

```
git clone git@gitlab.com:jay-huh/esp-idf.git ~/esp-idf

cd ~/esp-idf
./install.sh
. ./export.sh

```

### Define IDF_PATH and get_idf

```
# add follow lines into `~/.zshrc`
export IDF_PATH="$HOME/esp-idf"
alias get_idf=". ${IDF_PATH}/export.sh"

```

### Examples: Hello World

```
# Setup the environment variables
get_idf
  OR
. $IDF_PATH/export.sh

cd $IDF_PATH/examples/get-started/hello_world

# build
idf.py all
# flash burning
idf.py -p [PORT] flash
```

## AVR

```
# install avr-gcc and avr-gdb
brew tap osx-cross/avr

brew install avr-gcc avr-gdb
brew install avrdude
```

### Debugger

```
pip3 install --user pymcuprog

git@gitlab.com:jay-huh/pyavrdbg.git [clone_directory]
cd [clone_directory]
git checkout [custom branch]

echo 'export PATH="$PATH:[path_for_avrdbg]"' >> ~/.zshrc

# wait GDB session 127.0.0.1:12555
# python main.py
./pyavrdbg -d avr128da48 -p 12555

avr-gdb [elf file]
(gdb) target remote :12555

```
