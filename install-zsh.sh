#!/bin/bash

INSTALL_OH_MY_ZSH=

./uninstall.sh
./common.sh

## Install Oh-My-ZSH
if [ ! -z "${INSTALL_OH_MY_ZSH}" ] ; then
  #sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  wget -O ./install-omz.sh https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh
  chmod a+x install-omz.sh

  ./install-omz.sh
  rm -vf ./install-omz.sh
fi

if [ ! -L "$HOME/.zshrc" ] ; then
  mv -vn $HOME/.zshrc $HOME/.zshrc-org
  rm -f $HOME/.zshrc
fi

if [ ! -z "${INSTALL_OH_MY_ZSH}" ] ; then
  ZSH_CUSTOM="$HOME/.oh-my-zsh/custom"
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git \
    ${ZSH_CUSTOM}/plugins/zsh-syntax-highlighting

  git clone https://github.com/zsh-users/zsh-autosuggestions.git \
    ${ZSH_CUSTOM}/plugins/zsh-autosuggestions
fi

echo "Change shell to zsh...."
chsh -s $(which zsh)

stow -v -R -t ${HOME} --no-folding zsh

echo "[ NOTE!!!!!!!!! ]"
echo "[ VIM ] excute vim-plugin.sh for installing plugins and syntax highlighting."

./install-npm.sh
source ~/.zshrc

LOCAL_BIN_PATH=${HOME}/.local/bin
${LOCAL_BIN_PATH}/vim-plugin.sh

