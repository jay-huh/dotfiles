#!/bin/sh

# install nvm and npm
./install-npm.sh

# install commitizen
npm install -g commitizen
npm install -g cz-conventional-changelog
npm install -g cz-emoji-conventional cz-emoji

# install commitlint
npm install -g @commitlint/cli
npm install -g @commitlint/config-conventional

# install standard-version
npm install -g standard-version

if [ ! -e "$HOME/.czrc" ] ; then
  stow -v conventional-commit
fi
