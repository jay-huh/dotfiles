#!/bin/bash

export LOCAL_BIN_PATH=${HOME}/.local/bin

mkdir -p ${HOME}/.local ${HOME}/.config
rm -rf ${LOCAL_BIN_PATH}

stow -v -R -t ${HOME} --no-folding vim
stow -v -R -t ${HOME} home-dir
stow -v -R -t ${HOME}/.local _local

#stow -v -R -t ${HOME}/.config --no-folding _config
stow -v -R -t ${HOME}/.config _config

if [ ! -e "${LOCAL_BIN_PATH}/repo" ] ; then
echo -n "install repo\n"
  curl https://storage.googleapis.com/git-repo-downloads/repo -o ${LOCAL_BIN_PATH}/repo
  chmod a+x ${LOCAL_BIN_PATH}/repo
fi

echo "Install tmux plugin manager"
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

if [[ $OSTYPE != "darwin"* ]] ; then
  ${LOCAL_BIN_PATH}/install-lazygit.sh
  ${LOCAL_BIN_PATH}/install-starship.sh
fi

echo "[ GIT ] excute git-conf.sh for e-mail registration."
${LOCAL_BIN_PATH}/git-conf.sh
