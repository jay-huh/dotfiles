#!/bin/sh

./uninstall.sh
./common.sh

if [ ! -L "$HOME/.bashrc" ] ; then
  mv -vn $HOME/.bashrc $HOME/.bashrc-org
  rm -f $HOME/.bashrc
fi

if [ ! -L "$HOME/.profile" ] ; then
  mv -vn $HOME/.profile $HOME/.profile-org
  rm -f $HOME/.profile
fi

echo "Change shell to bash...."
chsh -s $(which bash)

stow -v -R -t ${HOME} --no-folding bash

echo "[ NOTE!!!!!!!!! ]"
echo "[ VIM ] excute vim-plugin.sh for installing plugins and syntax highlighting."

./install-npm.sh
source ~/.bashrc

LOCAL_BIN_PATH=${HOME}/.local/bin
${LOCAL_BIN_PATH}/vim-plugin.sh

