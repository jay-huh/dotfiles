#!/bin/bash

APT="apt-get"

if [[ -z "${CI}" ]] ; then
	APT="apt"
fi

git submodule update --init

cd dev-env
./development.sh
cd ..

sudo ${APT} ${APT_CACHE_OPTION} install -y -qq zoxide
sudo ${APT} ${APT_CACHE_OPTION} install -y -qq lsd
sudo ${APT} ${APT_CACHE_OPTION} install -y -qq fd-find

sudo add-apt-repository -y ppa:jonathonf/vim
sudo ${APT} update
sudo ${APT} ${APT_CACHE_OPTION} install -y -qq vim

GIT_DELTA_PKG="git-delta_0.17.0_amd64.deb"
wget https://github.com/dandavison/delta/releases/download/0.17.0/${GIT_DELTA_PKG}

sudo ${APT} ${APT_CACHE_OPTION} install -y -qq ./${GIT_DELTA_PKG}

# Ubuntu < 24.04 => install zoxide
#curl -sSfL https://raw.githubusercontent.com/ajeetdsouza/zoxide/main/install.sh | sh
