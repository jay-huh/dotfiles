#!/bin/bash

#set -e
#set -x # debug

#TEST="-n"

STOW_DIR="vim home-dir"
STOW_OPTION_DIR="bash zsh conventional-commit"

for dir in ${STOW_OPTION_DIR}
do
  stow -D ${TEST} $dir > /dev/null 2>&1
  if [[ "0" = $? ]] ; then
    STOW_DIR=${STOW_DIR}" $dir"
  fi
done

echo "STOW_DIR: ${STOW_DIR}"
for dir in ${STOW_DIR}
do
  stow -v -D ${TEST} $dir
done

if [[ "${OSTYPE}" == "linux"* ]] ; then
  echo "Change to $(which bash)"
  if [ ! -e "$HOME/.bashrc" ] ; then
    cp -v $HOME/.bashrc-org $HOME/.bashrc
  fi
  if [ ! -e "$HOME/.profile" ] ; then
    cp -v $HOME/.profile-org $HOME/.profile
  fi
  chsh -s $(which bash)
else
  echo "Change to $(which zsh)"
  if [ ! -e "$HOME/.zshrc" ] ; then
    cp -v $HOME/.zshrc-org $HOME/.zshrc
  fi
  chsh -s $(which zsh)
fi

# coc-settings.json
if [ -d "$HOME/.vim" ] ; then
  stow -v -D _vim -t ${HOME}/.vim
fi

# configs: starship, alacritty, tmux
if [ -d "$HOME/.config" ] ; then
  stow -v -D _config -t ${HOME}/.config
fi
if [ -d "$HOME/.local" ] ; then
  stow -v -D _local -t ${HOME}/.local
fi
