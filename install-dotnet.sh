# Install .NET CLI tools and runtime
sudo apt install dotnet8
# Mono is an open source implementation of Microsoft's .NET Framework
sudo apt install mono-complete
# `csharp-ls` is a hacky Roslyn-based LSP server for C#, as an alternative to omnisharp-roslyn.
dotnet tool install --global csharp-ls

