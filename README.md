# dotfiles

# MacOS

1. `install-zsh.sh`

- Install `zsh` and `zap` as pakage manager
- Install `fzf` and `starship`
- Install vim plugins

1. `install-conventional-commit.sh`

- install `npm` using `nvm` internally
- install `commitizen` and `commitlint`

1. `install-mac.sh`

- Install packages using `Brewfile`

## Ubuntu

1. `install-ubuntu.sh`

- Install pakages for development
- Install CLI tools
- Install latest Vim

1. `install-conventional-commit.sh`

- install `npm` using `nvm` internally
- install `commitizen` and `commitlint`

1. `install-zsh.sh`

- Install `zsh` and `zap` as pakage manager
- Install `fzf` and `starship`
- Install vim plugins

1. Install docker

- `./dev-env/script/docker.sh`
