# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.3.0](https://gitlab.com/jay-huh/dotfiles/compare/v0.2.0...v0.3.0) (2021-12-28)


### ⚠ BREAKING CHANGES

* **vimrc:** update `vimrc`

Signed-off-by: Jaeyeong Huh <jay.jyhuh@gmail.com>

### Features

* **common.sh:** alias to remove dangling docker images ([216b01e](https://gitlab.com/jay-huh/dotfiles/commit/216b01eb00479cf543509021ab37dd55eb3973bf))
* **commonrc.sh:** add alias for `ssh` and `cat` ([49b7cc0](https://gitlab.com/jay-huh/dotfiles/commit/49b7cc0662d52cb460d73deb14db0bf0f5b703ad))
* **ctags:** add script for Python with ctags and cscope ([cdd0db7](https://gitlab.com/jay-huh/dotfiles/commit/cdd0db79179babbd32ba8df55db88f8a8c148534))
* **git:** fetch/pull set defalut `prune` ([a5a6ff2](https://gitlab.com/jay-huh/dotfiles/commit/a5a6ff2737d5a2f8c339e14be70bab85ffa07735))
* **macos:** default python to version 3 ([5488639](https://gitlab.com/jay-huh/dotfiles/commit/54886396d7ffd6dbe5fc1102f2e4cb6d596b49a5))
* **tmux:** change history limit to 50k lines ([648d0dc](https://gitlab.com/jay-huh/dotfiles/commit/648d0dc6deb5cc3965512431598b69dff42329a4))
* **tmux:** update `status-line` style ([48e3553](https://gitlab.com/jay-huh/dotfiles/commit/48e355340a834bc41ac69b3d16c31dbeea71a309))
* **vimrc-test:** auto-completion ([bac6dc9](https://gitlab.com/jay-huh/dotfiles/commit/bac6dc92c17c4745d48beb60166045e6f4e26fd9))
* **vimrc:** [test] apply `noexpandtab` to `Makefile` ([ab695cc](https://gitlab.com/jay-huh/dotfiles/commit/ab695cc6cf3f989ea1f58694f0c73db76cc8aa84))
* **vimrc:** add `pydiction` for python auto-complete ([9be290f](https://gitlab.com/jay-huh/dotfiles/commit/9be290f6f011e1f221f4676a570b74466e92d6c3))
* **vimrc:** add `vim-header` and enable `Termdebug` ([21e5c13](https://gitlab.com/jay-huh/dotfiles/commit/21e5c1386b5ee61fd8ee11093c5c241bc51e144e))
* **vimrc:** add file `vimrc-test` for test script ([d6250e0](https://gitlab.com/jay-huh/dotfiles/commit/d6250e0329b5bdd38427605d61aedb7dfda94fb2))
* **vimrc:** syntax highlighting for C/C++ ([32666b3](https://gitlab.com/jay-huh/dotfiles/commit/32666b3979f36de318224f293e0723de2f04b130))
* **wsl:** add startup files for `wsl2` ([e7dd622](https://gitlab.com/jay-huh/dotfiles/commit/e7dd622e9b258cd6e63f9f81b68b5fb485c50dac))
* **zplug:** apply `zplug` instead of `oh-my-zsh` ([0a32990](https://gitlab.com/jay-huh/dotfiles/commit/0a329905ec3ffadc0538428401d1fe81abb666e3))
* **zsh:** update `zstyle` for auto suggestion ([28a7007](https://gitlab.com/jay-huh/dotfiles/commit/28a70072eb218bdae95133d3d9048fb7be7906b2))


### Bug Fixes

* **commonrc.sh:** `PATH` problem for `homebrew/bin` ([9714c08](https://gitlab.com/jay-huh/dotfiles/commit/9714c0831c5c1d176a9b0c3493c5c77d29f103b4))
* **git:** set pager to `less -F -X` and use `vimdiff` for merging ([d285ca5](https://gitlab.com/jay-huh/dotfiles/commit/d285ca5f3eb932fd7f0d85dee479ae757bafacb5))
* **mac:** apply `dircolors` ([b7f017d](https://gitlab.com/jay-huh/dotfiles/commit/b7f017d27bbb348fb3adc7f31c28ba25537e6988))
* **macos:** add homebrew path ([9e5a92d](https://gitlab.com/jay-huh/dotfiles/commit/9e5a92df39bf363b3a0d0b46fa5c2815210477b6))
* **putty:** update ultra-dark color for putty ([c68cdcd](https://gitlab.com/jay-huh/dotfiles/commit/c68cdcd9b41045187915ee64c30892f9f27104c8))
* **tmux.conf:** update `vi-mode` for selection ([6364d78](https://gitlab.com/jay-huh/dotfiles/commit/6364d780bce7db43caaff9c78914de1d1d82433e))
* **tmux:** normal window flags set to ' ' ([fb811dc](https://gitlab.com/jay-huh/dotfiles/commit/fb811dce8821c91ba4b3b85a08495279f4b448b5))
* **vimrc-test:** add `ALE` and `clang_complete` ([092c8cc](https://gitlab.com/jay-huh/dotfiles/commit/092c8cc5d085bea2e85c517591b59edb46a619a1))
* **vimrc:** change `vimrc` from `vimrc-test` ([bf284c9](https://gitlab.com/jay-huh/dotfiles/commit/bf284c94b73899a09f774ad3ac1da222a72e194c))
* **vimrc:** highlight color of `debugBreakpoint` set to RED ([32d6e01](https://gitlab.com/jay-huh/dotfiles/commit/32d6e0138ed9b45f118d908c69fa822293df781a))
* **vimrc:** update `completeopt` ([601f6d9](https://gitlab.com/jay-huh/dotfiles/commit/601f6d9d7c2b13bb1aa2b29bde36d4d7a01ad24b))
* **vimrc:** update `indent-guide` options ([995f381](https://gitlab.com/jay-huh/dotfiles/commit/995f381ec30ab4fe0d98dff3a783074deb8c41ea))
* **zsh:** conditional install `oh-my-zsh` ([6706dd2](https://gitlab.com/jay-huh/dotfiles/commit/6706dd2aa501c3fdf31521b3bf942fb464ec6860))
* **zshrc:** apply customized prompt with git integration ([fd47cc3](https://gitlab.com/jay-huh/dotfiles/commit/fd47cc33cfecb31e1eb40ff9e8630fd19729edca))
* **zshrc:** menu select highlighting and `vi-mode` key binding ([9d7ca75](https://gitlab.com/jay-huh/dotfiles/commit/9d7ca75eb813e4bc21bab4e549a567b6907eeb1c))

## [0.3.0](http://gitlab.com///compare/v0.2.0...v0.3.0) (2021-09-14)


### Features

* **common.sh:** alias to remove dangling docker images ([216b01e](http://gitlab.com///commit/216b01eb00479cf543509021ab37dd55eb3973bf))
* **commonrc.sh:** add alias for `ssh` and `cat` ([49b7cc0](http://gitlab.com///commit/49b7cc0662d52cb460d73deb14db0bf0f5b703ad))
* **ctags:** add script for Python with ctags and cscope ([cdd0db7](http://gitlab.com///commit/cdd0db79179babbd32ba8df55db88f8a8c148534))
* **git:** fetch/pull set defalut `prune` ([a5a6ff2](http://gitlab.com///commit/a5a6ff2737d5a2f8c339e14be70bab85ffa07735))
* **macos:** default python to version 3 ([5488639](http://gitlab.com///commit/54886396d7ffd6dbe5fc1102f2e4cb6d596b49a5))
* **vimrc:** add `pydiction` for python auto-complete ([9be290f](http://gitlab.com///commit/9be290f6f011e1f221f4676a570b74466e92d6c3))
* **wsl:** add startup files for `wsl2` ([e7dd622](http://gitlab.com///commit/e7dd622e9b258cd6e63f9f81b68b5fb485c50dac))
* **zplug:** apply `zplug` instead of `oh-my-zsh` ([0a32990](http://gitlab.com///commit/0a329905ec3ffadc0538428401d1fe81abb666e3))
* **zsh:** update `zstyle` for auto suggestion ([28a7007](http://gitlab.com///commit/28a70072eb218bdae95133d3d9048fb7be7906b2))


### Bug Fixes

* **git:** set pager to `less -F -X` and use `vimdiff` for merging ([d285ca5](http://gitlab.com///commit/d285ca5f3eb932fd7f0d85dee479ae757bafacb5))
* **mac:** apply `dircolors` ([b7f017d](http://gitlab.com///commit/b7f017d27bbb348fb3adc7f31c28ba25537e6988))
* **macos:** add homebrew path ([9e5a92d](http://gitlab.com///commit/9e5a92df39bf363b3a0d0b46fa5c2815210477b6))
* **putty:** update ultra-dark color for putty ([c68cdcd](http://gitlab.com///commit/c68cdcd9b41045187915ee64c30892f9f27104c8))
* **tmux:** normal window flags set to ' ' ([fb811dc](http://gitlab.com///commit/fb811dce8821c91ba4b3b85a08495279f4b448b5))
* **tmux.conf:** update `vi-mode` for selection ([6364d78](http://gitlab.com///commit/6364d780bce7db43caaff9c78914de1d1d82433e))
* **vimrc:** update `completeopt` ([601f6d9](http://gitlab.com///commit/601f6d9d7c2b13bb1aa2b29bde36d4d7a01ad24b))
* **zsh:** conditional install `oh-my-zsh` ([6706dd2](http://gitlab.com///commit/6706dd2aa501c3fdf31521b3bf942fb464ec6860))
* **zshrc:** apply customized prompt with git integration ([fd47cc3](http://gitlab.com///commit/fd47cc33cfecb31e1eb40ff9e8630fd19729edca))
* **zshrc:** menu select highlighting and `vi-mode` key binding ([9d7ca75](http://gitlab.com///commit/9d7ca75eb813e4bc21bab4e549a567b6907eeb1c))

## [0.2.0](http://gitlab.com///compare/v0.1.0...v0.2.0) (2021-06-29)


### Features

* **conventional-commit:** install `nvm` for `Linux` abd `macOS` ([fad60c5](http://gitlab.com///commit/fad60c54044a8602d2784560cdfbe9489c302010))


### Bug Fixes

* **iterm2:** `perl` warning about locale with `iTerm2` ([4023640](http://gitlab.com///commit/402364011181b3359bd2f7a28721c7c6c35e8e1d))

## [0.1.0](https://gitlab.com/jay-huh/dotfiles/compare/v0.0.1...v0.1.0) (2021-06-29)


### ⚠ BREAKING CHANGES

* **conventional-commit:** Initial release

Signed-off-by: Jaeyeong Huh <jay.jyhuh@gmail.com>

### Features

* **conventional-commit:** update install script and add `rc` files ([a9f70fa](https://gitlab.com/jay-huh/dotfiles/commit/a9f70fa59dd7ae0a821ba95be0c861cf0cb4bc66))

### 0.0.1 (2021-06-28)


### Features

* **commitizen:** add `cz-commitizen` and `standard-version` using `npm` ([fc16225](https://gitlab.com/jay-huh/dotfiles/commit/fc1622565b85903ce555ccebf114e17646302b2e))
* **commonrc:** add `commonrc.sh` and update `bashrc` ([9196d88](https://gitlab.com/jay-huh/dotfiles/commit/9196d88ed85a10cd8938a676760d8ace63beb1fe))
* **install:** setup for `git` abd `vim` ([8df5b0d](https://gitlab.com/jay-huh/dotfiles/commit/8df5b0d23c12a3e473f144362721d79ab3a10839))
* **mac:** add `zsh` and `install-mac.sh` ([d7546ae](https://gitlab.com/jay-huh/dotfiles/commit/d7546aea29cc0bde043b988d042e55f84eebab57))
* **putty:** add `.reg` file for `UltraDark` theme ([59214d7](https://gitlab.com/jay-huh/dotfiles/commit/59214d738b40b7e5c5905d824df813df9d6093c1))
* **readme:** commit test ([c7a6c3c](https://gitlab.com/jay-huh/dotfiles/commit/c7a6c3c099cd10d72df3e2a345fa638e80b4fa49))
* **vimrc:** Enable `solarized dark`. Add `install.sh` ([c901179](https://gitlab.com/jay-huh/dotfiles/commit/c901179f3cb7094ebac5e8edb2353d7a28bc549c))
* **zsh:** add newline and enable plugins ([4333077](https://gitlab.com/jay-huh/dotfiles/commit/4333077a38b28c7d2d1ff77f148c62da0ce36ea4))
* **zsh:** add plugins for autocompletion ([51ff48f](https://gitlab.com/jay-huh/dotfiles/commit/51ff48f5140468043726d0915604801806fe4fea))
* **zsh:** newline prompt and bold style command ([6b7ac29](https://gitlab.com/jay-huh/dotfiles/commit/6b7ac29bf8a920b00deda32f1302f37e35380d82))
* **zsh:** Update `$PATH` for `$HOME/bin` ([018b78a](https://gitlab.com/jay-huh/dotfiles/commit/018b78a6b6c6be05dc3e2e4a7efe36b9bc9b0246))
* **zsh:** use `colored-man-pages` instead of `most` ([9336135](https://gitlab.com/jay-huh/dotfiles/commit/93361353cb31499e5808ae115f6ca7343104fb3e))
* **zsh:** use `vi-mode` instead of `zsh-vi-mode` ([e9046de](https://gitlab.com/jay-huh/dotfiles/commit/e9046ded6a8511236be9c3c4852d049e77ee31a7))
* initial commit. using `GNU stow` ([f617ccd](https://gitlab.com/jay-huh/dotfiles/commit/f617ccdfc7b5b6ba019b64a76e6996ac25cdebad))


### Bug Fixes

* **install.sh:** update for installing ([e0c8c75](https://gitlab.com/jay-huh/dotfiles/commit/e0c8c75aeb38c781a572492fbfd273129ed15dba))
* **tmux, zsh:** nested tmux and `zsh-vi-mode` conflict with `fzf` ([fe6de0d](https://gitlab.com/jay-huh/dotfiles/commit/fe6de0d10fdbdbb0ef84dc90221790a1e9f6426d))
