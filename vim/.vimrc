" vim: set nospell foldmethod=marker foldlevel=0:

" zM: Close All Folds
" zR: Open All Folds

source ~/.config/vim/base.vim
source ~/.config/vim/plugin.vim
source ~/.config/vim/filetype.vim
source ~/.config/vim/keymap.vim
source ~/.config/vim/auto-command.vim
source ~/.config/vim/tags-cscope.vim
source ~/.config/vim/highlight.vim
source ~/.config/vim/colorscheme.vim

" packadd - Termdebug {{{
" Termdebug
" :packadd termdebug
" :TermDebug
"let g:termdebug_popup = 0
let g:termdebug_wide = 163
let g:termdebugger = "gdb-multiarch"
"packadd termdebug
" packadd - Termdebug }}}
" Load Global Config file {{{
" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif
" Load Global Config file }}}
" Local overrides {{{
let $LOCALFILE=expand("~/.vimrc_local")

if filereadable($LOCALFILE)
  source $LOCALFILE
endif
" Local overrides }}}

" FIXME: for Terminal.app on MacOS
"set notermguicolors
