#!/bin/sh

if [[ -f $(which npm) ]]; then
  echo "npm was installed already!!"
  exit 0
fi

# install `nvm`
mkdir -p ${HOME}/.nvm
wget -O ./install-nvm.sh https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh
chmod a+x install-nvm.sh
./install-nvm.sh
rm -vf ./install-nvm.sh

# prepare for using `nvm`
export NVM_DIR=${HOME}/.nvm
[ -s ${NVM_DIR}/nvm.sh ] && \. ${NVM_DIR}/nvm.sh  # This loads nvm
[ -s ${NVM_DIR}/bash_completion ] && \. ${NVM_DIR}/bash_completion  # This loads nvm bash_completion

# install nvm and npm
nvm install --lts
npm config set fund false
