" vim: set nospell foldmethod=marker foldlevel=0:

" {{{ Install Plug for plugin manager
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
" }}} Install Plug for plugin manager

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
  "  Plug 'godlygeek/tabular'
  " {{{ lightline
  Plug 'itchyny/lightline.vim'
  Plug 'mengelbrecht/lightline-bufferline'
  "set background=dark
  let g:lightline = {
        \ 'colorscheme': 'darcula',
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ],
        \             [ 'gitbranch', 'cocstatus', 'readonly', 'relativepath', 'modified' ] ]
        \ },
        \ 'component_function': {
        \   'gitbranch': 'FugitiveHead',
        \   'cocstatus': 'coc#status'
        \ },
        \ 'tabline': {
        \   'left': [ ['tabs'], ['buffers'] ],
        \   'right': [ ['close'] ]
        \ },
        \ 'tabline_separator': { 'left': '', 'right': '' },
        \ 'tabline_subseparator': { 'left': '', 'right': '' },
        \ 'tab': {
        \   'active': [ 'tabnum' ],
        \   'inactive': [ 'tabnum' ]
        \ },
        \ 'component_expand': {
        \   'buffers': 'lightline#bufferline#buffers',
        \   'tabs': 'lightline#tabs'
        \ },
        \ 'component_type': {
        \   'buffers': 'tabsel'
        \ },
        \ 'component': {
        \   'lineinfo': ' %3l:%-2v',
        \ },
        \ }
  " }}} lightline

  " {{{ Colorscheme
  Plug 'tinted-theming/base16-vim'
  Plug 'catppuccin/vim', { 'as': 'catppuccin' }
  Plug 'sainnhe/gruvbox-material'
  " Set contrast.
  " This configuration option should be placed before `colorscheme gruvbox-material`.
  " Available values: 'hard', 'medium'(default), 'soft'
  "let g:gruvbox_material_background = 'hard'
  " Available values:   `'material'`, `'mix'`, `'original'`
  let g:gruvbox_material_foreground = 'mix'
  " For better performance
  let g:gruvbox_material_better_performance = 1
  Plug 'morhetz/gruvbox'
  Plug 'arcticicestudio/nord-vim'
  Plug 'lifepillar/vim-solarized8'
  Plug 'tomasiser/vim-code-dark'
  "Plug 'https://gitlab.com/protesilaos/tempus-themes-vim.git'
  Plug 'hzchirs/vim-material'
  let g:material_style='oceanic'
  " }}} Colorscheme

  " vim-indent-guides - A vim plugin to display the indention levels with thin vertical lines {{{
  "Plug 'chrisbra/Colorizer'
  Plug 'nathanaelkane/vim-indent-guides'
  " Indent-guide: Toggling <Leader>ig
  let g:indent_guides_enable_on_vim_startup = 1
  let g:indent_guides_auto_colors = 0
  autocmd VimEnter,Colorscheme * hi IndentGuidesOdd ctermfg=238 ctermbg=NONE guifg=grey25 guibg=NONE
  autocmd VimEnter,Colorscheme * hi IndentGuidesEven ctermfg=238 ctermbg=236 guifg=grey25 guibg=grey15
  " Tab...
  autocmd VimEnter,Colorscheme * hi SpecialKey ctermfg=238 guifg=grey25
  autocmd VimEnter,Colorscheme * hi NonText ctermfg=88 guifg=#800000
  " }}} vim-indent-guides

  " Useful plugins {{{
  Plug 'dhruvasagar/vim-zoom'
  Plug 'dominikduda/vim_current_word'
  let g:vim_current_word#enabled = 1
  " Twins of word under cursor:
  let g:vim_current_word#highlight_twins = 1
  " The word under cursor:
  let g:vim_current_word#highlight_current_word = 1
  let g:vim_current_word#highlight_only_in_focused_window = 1
  let g:vim_current_word#excluded_filetypes = ['ruby']
  " === git plugin {{{
  Plug 'tpope/vim-fugitive'     "Need for airline
  Plug 'airblade/vim-gitgutter'
  " git plugin }}}

  " Plugin outside ~/.vim/plugged with post-update hook
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'
  " - down / up / left / right
  "let g:fzf_layout = { 'down': '40%' }
  "" Default fzf layout
  "" - Popup window (center of the screen)
  let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }

  "" - Popup window (center of the current window)
  "let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'relative': v:true } }

  "" - Popup window (anchored to the bottom of the current window)
  "let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'relative': v:true, 'yoffset': 1.0 } }
  " An action can be a reference to a function that processes selected lines
  function! s:build_quickfix_list(lines)
    call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
    copen
    cc
  endfunction

  let g:fzf_action = {
        \ 'ctrl-q': function('s:build_quickfix_list'),
        \ 'ctrl-t': 'tab split',
        \ 'ctrl-x': 'split',
        \ 'ctrl-v': 'vsplit' }
  " Customize fzf colors to match your color scheme
  "let g:fzf_colors = {
  "      \ 'preview-fg':      ['fg', 'Normal'],
  "      \ 'preview-bg':      ['bg', 'Normal'],
  "      \ 'fg':      ['fg', 'Normal'],
  "      \ 'bg':      ['bg', 'Normal'],
  "      \ 'hl':      ['fg', 'Comment'],
  "      \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  "      \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  "      \ 'hl+':     ['fg', 'Search', 'Statement'],
  "      \ 'info':    ['fg', 'PreProc'],
  "      \ 'border':  ['fg', 'Ignore'],
  "      \ 'prompt':  ['fg', 'Conditional'],
  "      \ 'pointer': ['fg', 'Exception'],
  "      \ 'marker':  ['fg', 'Keyword'],
  "      \ 'spinner': ['fg', 'Label'],
  "      \ 'header':  ['fg', 'Comment']
  "      \ }
  Plug 'yuki-yano/fzf-preview.vim', { 'branch': 'release/rpc' }
  nmap <leader>f [fzf-p]
  xmap <leader>f [fzf-p]
  nnoremap <silent> [fzf-p]p     :<C-u>FzfPreviewFromResourcesRpc project_mru git<CR>
  nnoremap <silent> [fzf-p]gs    :<C-u>FzfPreviewGitStatusRpc<CR>
  nnoremap <silent> [fzf-p]ga    :<C-u>FzfPreviewGitActionsRpc<CR>
  nnoremap <silent> [fzf-p]b     :<C-u>FzfPreviewBuffersRpc<CR>
  nnoremap <silent> [fzf-p]B     :<C-u>FzfPreviewAllBuffersRpc<CR>
  nnoremap <silent> [fzf-p]o     :<C-u>FzfPreviewFromResourcesRpc buffer project_mru<CR>
  nnoremap <silent> [fzf-p]<C-o> :<C-u>FzfPreviewJumpsRpc<CR>
  nnoremap <silent> [fzf-p]g;    :<C-u>FzfPreviewChangesRpc<CR>
  nnoremap <silent> [fzf-p]/     :<C-u>FzfPreviewLinesRpc --add-fzf-arg=--no-sort --add-fzf-arg=--query="'"<CR>
  nnoremap <silent> [fzf-p]*     :<C-u>FzfPreviewLinesRpc --add-fzf-arg=--no-sort --add-fzf-arg=--query="'<C-r>=expand('<cword>')<CR>"<CR>
  nnoremap          [fzf-p]gr    :<C-u>FzfPreviewProjectGrepRpc<Space>
  xnoremap          [fzf-p]gr    "sy:FzfPreviewProjectGrepRpc<Space>-F<Space>"<C-r>=substitute(substitute(@s, '\n', '', 'g'), '/', '\\/', 'g')<CR>"
  nnoremap <silent> [fzf-p]t     :<C-u>FzfPreviewBufferTagsRpc<CR>
  nnoremap <silent> [fzf-p]q     :<C-u>FzfPreviewQuickFixRpc<CR>
  nnoremap <silent> [fzf-p]l     :<C-u>FzfPreviewLocationListRpc<CR>

  Plug 'yuttie/comfortable-motion.vim' " smooth scrolling 'C-d' or 'C-u'
  let g:comfortable_motion_friction = 200.0
  let g:comfortable_motion_air_drag = 16.0
  "let g:comfortable_motion_no_default_key_mappings = 1
  "let g:comfortable_motion_scroll_down_key = "j"
  "let g:comfortable_motion_scroll_up_key = "k"
  Plug 'christoomey/vim-tmux-navigator'
  "let g:tmux_navigator_no_mappings = 1
  "noremap <silent> <C-s>h :<C-u>TmuxNavigateLeft<cr>
  "noremap <silent> <C-s>j :<C-u>TmuxNavigateDown<cr>
  "noremap <silent> <C-s>k :<C-u>TmuxNavigateUp<cr>
  "noremap <silent> <C-s>l :<C-u>TmuxNavigateRight<cr>
  "noremap <silent> <C-s>\ :<C-u>TmuxNavigatePrevious<cr>
  "Plug 'preservim/nerdtree'
  "  let NERDTreeShowHidden = 1
  Plug 'preservim/nerdcommenter'
  Plug 'terryma/vim-multiple-cursors'
  Plug 'Raimondi/delimitMate'
  let delimitMate_expand_inside_quotes = 1
  let delimitMate_expand_cr = 1
  let delimitMate_expand_space = 1


  " === vim-header {{{
  Plug 'alpertuna/vim-header'
  let g:header_auto_add_header = 0
  let g:header_field_filename = 0
  let g:header_field_modified_timestamp = 0
  let g:header_field_modified_by = 0
  function! s:GetGitUserName()
    let _author = get( g:, 'header_field_author' )
    if empty(_author)
      let _author = system( 'git config --get user.name' )
      let _author = strpart( _author, 0, strlen( _author ) - 1 )
    endif
    return _author
  endfunction
  function! s:GetGitUserEmail()
    let _email = get( g:, 'header_field_author_email' )
    if empty(_email)
      let _email = system( 'git config --get user.email' )
      let _email = strpart( _email, 0, strlen( _email ) - 1 )
    endif
    return _email
  endfunction

  let g:header_field_author = s:GetGitUserName()
  let g:header_field_author_email = s:GetGitUserEmail()
  let g:header_field_timestamp_format = '%Y-%m-%d %T %Z'
  "let g:header_field_license_id = 'MIT'
  let g:header_field_copyright = 'Copyright (c) ' . strftime('%Y')
        \ . ', Jay Huh
        \ - All Rights Reserved'
  map <F4> :AddHeader<CR>
  " === vim-header }}}
  "Plug 'vim-scripts/MultipleSearch'
  "Plug 'vim-scripts/ctrlp.vim'
  "Plug 'vim-scripts/ack.vim'
  "Plug 'ervandew/supertab'
  Plug 'junegunn/rainbow_parentheses.vim'
  let g:rainbow#max_level = 16
  let g:rainbow#pairs = [['{', '}'],['(', ')'], ['[', ']']]
  " List of colors that you do not want. ANSI code or #RRGGBB
  let g:rainbow#blacklist = [232,233,234,235,236,237,238,239,240,241,242,243,
        \ 244,245,246,247,248,249,250,251,252,253,254,255]
  autocmd VimEnter * RainbowParentheses
  Plug 'bignimbus/you-are-here.vim'
  "nnoremap <silent> <leader>here :call you_are_here#Toggle()<CR>
  nnoremap <silent> <leader>here :call you_are_here#ToggleFor(2500)<CR>
  " Useful plugins }}}

  " Syntax Highlighting {{{
  " === vim-cpp-highlight {{{
  Plug 'bfrg/vim-cpp-modern'
  " Disable function highlighting (affects both C and C++ files)
  let g:cpp_function_highlight = 1
  " Enable highlighting of C++11 attributes
  let g:cpp_attributes_highlight = 1
  " Highlight struct/class member variables (affects both C and C++ files)
  let g:cpp_member_highlight = 1
  " Put all standard C and C++ keywords under Vim's highlight group 'Statement'
  " (affects both C and C++ files)
  let g:cpp_simple_highlight = 0
  " === vim-cpp-highlight }}}

  "Plug 'rkulla/pydiction'
  "  let g:pydiction_location = '~/.vim/plugged/pydiction/complete-dict'
  "  let g:pydiction_menu_height = 5
  Plug 'preservim/vim-markdown'
  Plug 'stephpy/vim-yaml'
  Plug 'ekalinin/dockerfile.vim'
  " Syntax Highlighting }}}

  " {{{ Code Analysis and Syntax checking
  "Plug 'jiangmiao/auto-pairs'
  "Plug 'scrooloose/syntastic'
  " ALE Settings {{{
  Plug 'w0rp/ale'
  let g:ale_linters_explicit = 1
  let g:ale_completion_delay = 500
  let g:ale_echo_delay = 20
  let g:ale_lint_delay = 500
  let g:ale_set_highlights = 1
  "let g:ale_sign_error ='>✗'
  "let g:ale_sign_warning ='-‼'
  let g:ale_echo_msg_error_str = 'ERR'
  let g:ale_echo_msg_warning_str = 'WRN'
  let g:ale_echo_msg_format = '[%severity%][%linter%] %code: %%s'

  let g:ale_completion_enabled = 0
  ""    let g:ale_c_parse_makefile = 1  " Default: 0
  ""    let g:ale_c_parse_compile_commands = 1  " Default: 1
  ""    let g:ale_c_always_make = 0 " Default: 1

  let g:ale_c_cc_executable = 'gcc'   " or `clang`
  let g:ale_cpp_cc_executable = 'g++' " or `clang`
  let g:ale_c_cc_options= '-Wall -O2 -std=c99'
  let g:ale_cpp_cc_options= '-Wall -O2 -std=c++14'

  let g:ale_c_cppcheck_options= '--enable=style,unusedFunction'
  let g:ale_cpp_cppcheck_options= '--enable=style,unusedFunction'

  let g:ale_c_clangformat_style_options = 'google'
  let g:ale_cpp_clangformat_style_options = 'google'
  let g:ale_c_clangformat_use_local_file = 1
  let g:ale_cpp_clangformat_use_local_file = 1
  "let g:ale_c_clangformat_options = '-style=google'
  "let g:ale_cpp_clangformat_options = '-style=google'

  "let g:ale_linters_explicit = 1
  " c/cpp: `cc`, `clangd`, `cppcheck`, `clangtidy`
  let g:ale_linters = {
        \ 'c' : [ 'cppcheck' ],
        \ 'cpp' : [ 'cppcheck' ],
        \ }
  let g:ale_fixers = {
        \ '*' : [ 'remove_trailing_lines', 'trim_whitespace' ],
        \ 'c' : [ 'clang-format' ],
        \ 'cpp' : [ 'clang-format' ],
        \ }
  " with CoC.nvim
  " :CocConfig => "diagnostic.displayByAle"
  let g:ale_disable_lsp = 1
  " ALE Settings }}}
  " CoC Settings {{{
  Plug 'neoclide/coc.nvim', { 'branch': 'release' }
  "let g:coc_global_extensions = [ 'coc-snippets', 'coc-vimlsp',
  "      \ 'coc-clangd', 'coc-sh', 'coc-markdownlint' ]
  let g:coc_snippet_next = '<TAB>'
  let g:coc_snippet_prev = '<S-TAB>'
  let g:coc_status_error_sign = 'E: '
  let g:coc_status_warning_sign = 'W: '

  " Insert mode completion - only complete the longest common part of the text
  " <C-x><C-o>: open popup from clang_complete
  " <C-n>/<C-p>: open popup from omnifunc
  " <C-y>: Insert popup selection and exit
  " <C-e>: Cancel popup selection
  "<CR> to confirm completion, use: >
  "inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<CR>"
  inoremap <expr> <C-k> coc#pum#visible() ? coc#pum#prev(1) : (pumvisible() ? "\<C-p>" : "\<C-k>")
  inoremap <expr> <C-j> coc#pum#visible() ? coc#pum#next(1) : (pumvisible() ? "\<C-n>" : "\<C-j>")

  "" Make <CR> to accept selected completion item or notify coc.nvim to format
  "" <C-g>u breaks current undo, please make your own choice
  "inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
  "                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
  inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
        \: (pumvisible() ? "\<C-y>" : "\<CR>")

  "" Use <c-space> to trigger completion
  "if has('nvim')
  "  inoremap <silent><expr> <c-space> coc#refresh()
  "else
  "  inoremap <silent><expr> <c-@> coc#refresh()
  "endif

  " Use `[g` and `]g` to navigate diagnostics
  " Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
  nmap <silent> [g <Plug>(coc-diagnostic-prev)
  nmap <silent> ]g <Plug>(coc-diagnostic-next)

  " GoTo code navigation.
  nmap <silent> gd <Plug>(coc-definition)
  nmap <silent> gy <Plug>(coc-type-definition)
  nmap <silent> gi <Plug>(coc-implementation)
  nmap <silent> gr <Plug>(coc-references)

  " Use K to show documentation in preview window.
  nnoremap <silent> K :call ShowDocumentation()<CR>

  function! ShowDocumentation()
    if CocAction('hasProvider', 'hover')
      call CocActionAsync('doHover')
    else
      call feedkeys('K', 'in')
    endif
  endfunction

  "" Highlight the symbol and its references when holding the cursor.
  "autocmd CursorHold * silent call CocActionAsync('highlight')

  " Symbol renaming.
  nmap <leader>rn <Plug>(coc-rename)

  " Formatting selected code
  xmap <leader>f  <Plug>(coc-format-selected)
  nmap <leader>f  <Plug>(coc-format-selected)

  augroup mygroup
    autocmd!
    " Setup formatexpr specified filetype(s)
    autocmd FileType * setl formatexpr=CocAction('formatSelected')
    " Update signature help on jump placeholder
    autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
  augroup end

  " Applying code actions to the selected code block
  " Example: `<leader>aap` for current paragraph
  xmap <leader>a  <Plug>(coc-codeaction-selected)
  nmap <leader>a  <Plug>(coc-codeaction-selected)

  " Remap keys for applying code actions at the cursor position
  nmap <leader>ac  <Plug>(coc-codeaction-cursor)
  " Remap keys for apply code actions affect whole buffer
  nmap <leader>as  <Plug>(coc-codeaction-source)
  " Apply the most preferred quickfix action to fix diagnostic on the current line
  nmap <leader>qf  <Plug>(coc-fix-current)

  " Remap keys for applying refactor code actions
  nmap <silent> <leader>re <Plug>(coc-codeaction-refactor)
  xmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)
  nmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)

  " Run the Code Lens action on the current line
  nmap <leader>cl  <Plug>(coc-codelens-action)

  " Map function and class text objects
  " NOTE: Requires 'textDocument.documentSymbol' support from the language server
  xmap if <Plug>(coc-funcobj-i)
  omap if <Plug>(coc-funcobj-i)
  xmap af <Plug>(coc-funcobj-a)
  omap af <Plug>(coc-funcobj-a)
  xmap ic <Plug>(coc-classobj-i)
  omap ic <Plug>(coc-classobj-i)
  xmap ac <Plug>(coc-classobj-a)
  omap ac <Plug>(coc-classobj-a)

  " CoC Settings }}}
  " }}} Code Analysis and Syntax checking

  " {{{ snippets
  Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
  let g:UltiSnipsEditSplit="vertical"
  let g:UltiSnipsExpandTrigger="<Tab>"
  "let g:UltiSnipsJumpForwardTrigger="<C-n>"  " default: <C-j>
  "let g:UltiSnipsJumpBackwardTrigger="<C-p>" " default: < C-k>
  " }}} snippets
call plug#end()

