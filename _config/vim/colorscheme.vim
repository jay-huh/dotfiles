" vim: set nospell foldmethod=marker foldlevel=0:

set background=dark
if (has("termguicolors"))
  if $TERM_PROGRAM != "Apple_Terminal"
    set termguicolors
  endif
endif
" ColorScheme {{{
"colorscheme solarized8_flat
colorscheme catppuccin_mocha
"colorscheme gruvbox-material
"colorscheme gruvbox
"if (has('gui_running'))
"  " NOTE: Comment out for 256 color on terminal.
"  if (has("termguicolors"))
"    set termguicolors
"  endif
"  colorscheme gruvbox
"endif

" follow base16-shell settings
if (!has("nvim"))
  if filereadable(expand("$HOME/.config/tinted-theming/set_theme.vim"))
    "let base16colorspace=256
    source $HOME/.config/tinted-theming/set_theme.vim
  else
    if exists('$BASE16_THEME')
          \ && (!exists('g:colors_name')
          \ || g:colors_name != 'base16-$BASE16_THEME')
      let base16colorspace=256
      let g:colors_name="base16-".expand("$BASE16_THEME")
      execute "colorscheme ".g:colors_name
    endif
  endif
endif
" vimdiff colorscheme
if &diff
  colorscheme gruvbox-material
endif
" ColorScheme }}}
