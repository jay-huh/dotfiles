" vim: set nospell foldmethod=marker foldlevel=0:

" zM: Close All Folds
" zR: Open All Folds

" Init {{{
  " This line should not be removed as it ensures that various options are
  " properly set to work with the Vim-related packages available in Debian.
  "runtime! debian.vim

  syntax on
  set nocompatible              " be iMproved, required
  "filetype off                  " required
  set gfn=MesloLGS_Nerd_Font:h14

  filetype plugin indent on
  set number
  set relativenumber
  syntax on
  set history=500
  set showmode
  set showmatch     " Show matching brackets.
  " Show commands as they are being typed (e.g. 2dd), also the number of lines
  " or the dimensions in Visual selection modes
  set showcmd
  " Tab bar display setting: 0: never, 1: only when >1 tabs, 2: always
  set showtabline=2
  set laststatus=2  " always on
  " Show a column at textwidth+1 chars as the boundary of textwidth
  set colorcolumn=81
  " When wrapping, break at end of word rather than mid-word
  set wrap
  set linebreak

" Init }}}
" General Settings {{{
  set foldcolumn=0  "Show folds in the gutter
  set updatetime=300 " default 4000
  " Don't redraw the screen while performing macros (so they run faster)
  set lazyredraw
  " Always show the sign column so things don't jump around when it appears
  set signcolumn=auto
  set mouse=a

  " Highlight the line that the cursor is on
  set cursorline
  "set cursorcolumn

  set exrc
  set secure

  set fileformats=unix,dos,mac
  set encoding=utf-8
  set fileencoding=utf-8
  set fileencodings=utf-8,cp949,euc-kr
  " Undo {{{
  if has('persistent_undo')
    if !isdirectory($HOME . '/.vim/undo')
      call mkdir($HOME . '/.vim/undo', 'p')
    endif

    set undodir=~/.vim/undo
    set undofile
    set undolevels=1000
    set undoreload=10000
  endif
  " Undo }}}
  " Backups {{{
  if !isdirectory($HOME . '/.vim/backups')
    call mkdir($HOME . '/.vim/backups', 'p')
  endif

  set backupdir=~/.vim/backups
  "set backup
  set nobackup
  set nowritebackup
  " Backups }}}
  " Swap Files {{{
  if !isdirectory($HOME . '/.vim/swaps')
    call mkdir($HOME . '/.vim/swaps', 'p')
  endif

  set directory=~/.vim/swaps
  set noswapfile
  " Swap Files }}}
" General Settings }}}
" Vim Only config {{{
  if !has("nvim")
    " Cursor Shape {{{
    " Change cursor shape between insert and normal mode
    "Cursor settings:
    "  1 -> blinking block
    "  2 -> solid block
    "  3 -> blinking underscore
    "  4 -> solid underscore
    "  5 -> blinking vertical bar
    "  6 -> solid vertical bar
    "Mode Settings
    if &term =~ "xterm"
      let &t_SI = "\<Esc>]12;red\x7"
      let &t_SR = "\<Esc>]12;red\x7"
      let &t_EI = "\<Esc>]12;red\x7"
    endif
    let &t_SI.="\e[5 q" "SI = INSERT mode
    let &t_SR.="\e[4 q" "SR = REPLACE mode
    let &t_EI.="\e[1 q" "EI = NORMAL mode (ELSE)
    " Cursor Shape }}}
    " Speed Improvements {{{
    set ttyfast
    " syntax sync minlines=200
    " syntax sync maxlines=240
    set synmaxcol=210 " Prevent vim from processing syntax on really long lines
    " Speed Improvements }}}
    " Reload files changed outside of vim
    set autoread
    " Replace annoying error bell with annoying screen flash
    set visualbell
    " Make the visual bell do nothing (so it's not annoying)
    set t_vb=
    " Set term title
    set title
    " Get rid of the pesky 'Thanks for flying Vim' title on exit
    let &titleold=''
    " Restore previous buffers when starting
    "set viminfo+=%
    set autowrite     " Automatically save before commands like :next and :make
    "	Vim 256 Color setting
    set t_Co=256
    set t_AB=[48;5;%dm
    set t_AF=[38;5;%dm
    " Italic
    set t_ZH=[3m
    set t_ZR=[23m
    "set report=0   " Threshold for reporting number of lines changed.
    set statusline=%<%n:\ %t\ (%f)\ ascii=%b\ %=[%{&ff}:%{&fenc}]%h%r%w%y%m\ %-20(col:\ %c%V\ line:\ %l/%L%)\ (%P)
    "set selection=exclusive   "  커서 위치 문자 선택에 미포함.
    "set whichwrap=h,l,<,>,[,]
    "set wildmode=longest:full,full
  endif
" Vim Only config }}}
" Moving Around {{{
  " Start vertically scrolling when 3 lines from the top or bottom
  set scrolloff=5

  " Start horizontally scrolling when 3 lines from the edges
  set sidescrolloff=3

  " How many columns to scroll at a time horizontally
  set sidescroll=1

  " Don't reset cursor to start of line when moving around
  set nostartofline
" Moving Around }}}
" Splits {{{
  " Open new split below rather than above
  set splitbelow

  " Open new vertical split to the right, rather than left
  set splitright
" Splits }}}
" Searching {{{
  " Highlight search matches
  set hlsearch

  " Start searching while typing
  set incsearch

  " NeoVim: Show what substitutions will look like real-time
  "set inccommand=nosplit

  " Enable regex for searches
  set magic

  " Case insensitive searches...
  set ignorecase

  " ...unless specifically searching for something with uppercase characters
  set smartcase
" Searching }}}
" Copy and Paste {{{
  if has("clipboard")
    " use only the system clipboard
    set clipboard=unnamedplus
    if has("macunix")
      set clipboard=unnamed
    endif
  else
    " Use xclip for copy/paste to/from system clipboard
    vmap "+y :!xclip -f -sel -i<CR>
    map  "+p :r!xclip -o -sel <CR>
  endif

  " Leave paste mode when leaving insert mode
  augroup copypaste
    autocmd!
    autocmd InsertLeave * set nopaste
  augroup end
" Copy and Paste }}}
" Text Editing and Formatting {{{
  " Allow unwritten buffers in the background
  set hidden

  " Allow full backspace in insert mode
  set backspace=indent,eol,start
  " Only insert single space after '.', '?', and '!' when joining
  set nojoinspaces

  " Treat strings starting with '0' as numbers for the purposes of incrementing.
  "set nrformats-=octal

  " Reset formatoptions: default 'croql'
  set formatoptions=

  " Format comments
  set formatoptions+=c

  " Continue comments onto next line
  set formatoptions+=r

  " Format comments with gq
  set formatoptions+=q

  " Recognize numbered lists
  set formatoptions+=n

  " Use indent from 2nd line of a paragraph
  "set formatoptions+=2

  " Don't break lines that are already long
  set formatoptions+=l

  " Break before 1-letter words
  set formatoptions+=1

  " Delete comment character when joining commented lines
  if v:version > 703 || v:version == 703 && has("patch541")
      set formatoptions+=j
  endif

  " Don't comment newline when using o or O from a commented line (needs
  " autocmd otherwise it gets overwritten)
  augroup formatting
      autocmd!
      autocmd FileType * setlocal formatoptions-=o
  augroup END
" Text Editing and Formatting }}}
" Tabs & Indentation {{{
  "Tab control
  " gt : ":tabnext"
  " gT : ":tabprevious"
  " g<Tab>: ":tab last"
  " :tabedit [file]
  nmap  <leader>tn    :tabnew<CR>
  nmap  <leader>tc    :tabclose<CR>
  "nmap  <Tab> :tabnext<CR>

  " Buffer control
  nmap <leader>bn     :bnext<CR>
  nmap <leader>bp     :bprevious<CR>
  nmap <leader>bd     :bdelete<CR>

  " Resize Window
  nnoremap <C-Up>    :resize +2<CR>
  nnoremap <C-Down>  :resize -2<CR>
  nnoremap <C-Left>  :vertical resize -2<CR>
  nnoremap <C-Right> :vertical resize +2<CR>

  " Open gitfugitive in new buffer
  "nmap  <leader>gg   :Gtabedit :<CR>
  nmap  <leader>gg   :tabnew \|:Git \|:wincmd o<CR>

  " Use spaces instead of tabs
  set expandtab

  " How many characters wide the tab character should be
  "set tabstop=4

  " How many spaces to use instead of a tab
  "set shiftwidth=4

  " Intelligently backspace the right number of space characters
  set smarttab

  " Copy indent level from previous line when starting a new line
  set autoindent

  " Seems to automatically update the indentation when a closing paren is typed
  set smartindent

  " Copy whatever characters were used to indent the previous line
  "set copyindent

  " Preserve as much of the existing indentation characters when changing indentation level
  "set preserveindent

  set cindent
  set cinoptions=g0,0,l1,t0
  " display tabs, tailing spaces, and other chars visually
  set list

  " Tab: |,▶,▸,→,┣━
  "set listchars=tab:▸\ ,space:·,trail:·,extends:→,precedes:←,nbsp:␣,eol:↲
  "set listchars=tab:→\ ,trail:·,extends:→,precedes:←,nbsp:␣
  set list listchars=tab:\|\ ,trail:·,extends:>,precedes:<,nbsp:␣
  set fillchars+=fold:\ ,vert:│
  set showbreak=↪\

" Tabs & Indentation }}}
" File Browsing {{{
  " Search in all subdirectories
  " default path=.,/usr/include,,
  if has("macunix")
    " Find system c/c++ include path: `clang -v -x c/c++ - -E -`
    set path=.,/opt/homebrew/opt/llvm/include/c++/v1,,
  endif
  set path+=**

  "let g:netrw_banner = 0 " Disable the banner
  "let g:netrw_liststyle = 3   " Tree style
  "let g:netrw_browse_split = 4
  let g:netrw_altv = 1
  let g:netrw_winsize = 25
  "let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
  "let g:netrw_keepdir = 0

  " Force netrw to open links directly in firefox rather than using xdg-open
  " and friends. This fixes an issue where xdg-open and friends will redirect
  " to the login screen for URLs where a session exists in Firefox.
  "let g:netrw_browsex_viewer = "firefox"
" File Browsing }}}
" Completion {{{
  " Command mode completetion
  set wildmenu

  " Only complete the longest common part of the text, and show the wildmenu
  " if enabled. If tab is pressed again, it will cycle through the options.
  "set wildmode=list:longest,full
  set wildmode=longest,full
  " Ignore files
  set wildignore+=**/.git/*
  set wildignore+=*.pyc
  set wildignore+=**/node_modules/*

  " Insert mode completion - only complete the longest common part of the text
  set completeopt=menuone,noinsert,noselect " ,popup
  set complete-=i " except include files. default: ".,w,b,u,t,i"
  "set conceallevel=2
  "set concealcursor=vin
  set pumheight=20
  set omnifunc=syntaxcomplete#Complete

  "c    don't give |ins-completion-menu| messages.  For example,
  "-- XXX completion (YYY)", "match 1 of 2", "The only match",
  "Pattern not found", "Back at original", etc.
  set shortmess+=c

" Completion }}}
" Spell Checking {{{
  " Enable spell checking
 set nospell

  " Self-explanatory
  set spelllang=en_us
" }}}
