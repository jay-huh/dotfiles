" vim: set nospell foldmethod=marker foldlevel=0:

" Whitespace {{{
augroup HighlightWhitespace
  autocmd!
  autocmd VimEnter,Winenter,ColorScheme * highlight ExtraWhitespace ctermbg=238 guibg=#505050
  autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
  autocmd InsertLeave * match ExtraWhitespace /\s\+$/
  autocmd VimEnter,Winenter * match ExtraWhitespace /\s\+$/
augroup end
" Whitespace }}}
augroup GeneralAutoCommands
  autocmd!
  autocmd VimEnter * normal !echo
"  " Return to last edit position when opening files
"  autocmd BufReadPost * call setpos(".", getpos("'\""))
"
"  " Automatically re-source the .vimrc on save
"  "autocmd! BufWritePost $MYVIMRC source % | echom "Reloaded " . $MYVIMRC | redraw
"
"  " Add a variation for when we edit the .vimrc while using neovim where
"  " $MYVIMRC is set to init.vim
"  "autocmd! BufWritePost .vimrc source % | echom "Reloaded " . $MYVIMRC | redraw
"
"  " Use an interactive shell (to get zsh functions and aliases)
"  "autocmd vimenter * let &shell='/bin/zsh -i'
augroup end
"
"" Create any required directories when saving
"function s:MkNonExDir(file, buf)
"  if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
"    let dir=fnamemodify(a:file, ':h')
"    if !isdirectory(dir)
"      call mkdir(dir, 'p')
"    endif
"  endif
"endfunction
"augroup BWCCreateDir
"  autocmd!
"  autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
"augroup END
