" vim: set nospell foldmethod=marker foldlevel=0:

" n - Normal
" v - Visual and Select
" s - Select
" x - Visual
" o - Operator-pending
" i - Insert
" l - Insert, Command-line, Lang-Arg
" c - Command-line
" None - Normal, Visual, Select, Operator-pending
"
" [nvsxoilc]noremap - Don't recursively map if recursive mapping is on (default)
" [nvsxoilc]map
" [nvsxoilc]unmap
" [nvsxoilc]mapclear

" Leader {{{
" Use the default leader
let mapleader = "\\"
let g:mapleader = "\\"
"let mapleader=','

" Map space to be a leader key as well (so that showcmd works for space)
map <Space> <Leader>
" LEader }}}
" HHKB Upecific {{{
"vnoremap `  <esc>  " HHKB: ESC remapped as backtick()
" HHKB Specific }}}

" netrw: Left Explorer Toggle
nnoremap <leader>e  :Lexplore<CR>
map      <F9>       <ESC>:Lexplore<CR>

" Quickly edit our .vimrc file
nmap <leader>ve :e ~/.vimrc<cr>
" Copy current file path to clipboard
nnoremap <Leader>c :let @+=expand('%:p')<CR>

"noremap <leader>f ]}v[{zf
"noremap <leader><leader> zf%
"
"map ,noi :set noai<CR>:set nocindent<CR>:set nosmartindent<CR>
"map ,sei :set ai<CR>:set cindent<CR>:set smartindent<CR>

map <silent> <esc><esc> :nohlsearch<CR>
" Insert Datetime
imap  <silent>  <C-T><C-T> <C-R>=strftime("%Y-%m-%d %T")<CR>

" Quicky escape to normal mode
"inoremap jj <esc>

fun! SetupCommandAlias(from, to)
  exec 'cnoreabbrev <expr> '.a:from
        \ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:from.'")'
        \ .'? ("'.a:to.'") : ("'.a:from.'"))'
endfun
" :W to :w
call SetupCommandAlias("W","w")
" vertical sprit for cscope
call SetupCommandAlias("vcs","vert scs")

"set cscopequickfix=s-,c-,d-,i-,t-,e-,a-
"set cscopetag " cst

" Behave Vim: yank to end of line
nnoremap Y yg$

" Keeping it centered
" zz: redraw, cursor line at center of window
" zv:open enough folds to view the cursor line
" m[z]: Set mark
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

" Moving Text, and indent
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
"inoremap <C-j> <esc>:m .+1<CR>== i
"inoremap <C-k> <esc>:m .-2<CR>== i
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==

" Zoom
" TODO: goto current line
" #: the last accessed tab page
" $: the last tab page
"nnoremap <leader>z mz \| :tabedit %<CR> \| `z
nnoremap <leader>z mZ \| :$tab split<CR>zz
nnoremap <leader>Z :tabnext # \| :tabclose #<CR>

" vertical split window for file under cursor
" <C-W>f => split window
" override <C-W><C-F> => default is same as <C-W>f
map <C-W><C-F> :vertical wincmd f<CR>
"imap <C-W><C-F> <ESC>:vertical wincmd f<CR>

" Show current line's git log
map <silent><Leader>g :call setbufvar(winbufnr(popup_atcursor(systemlist("cd " . shellescape(fnamemodify(resolve(expand('%:p')), ":h")) . " && git log --no-merges -n 1 -L " . shellescape(line("v") . "," . line(".") . ":" . resolve(expand("%:p")))), { "padding": [1,1,1,1], "pos": "botleft", "wrap": 0 })), "&filetype", "git")<CR>

"  nmap <leader>w :w<cr>
"
"  " Auto change directory to match current file ,cd
"  nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>
"
"  "nmap <leader>l :setlocal number!<CR>:setlocal list!<CR>:silent! GitGutterToggle<CR>:silent! setlocal relativenumber!<CR>:silent! IndentLinesToggle<CR>
"
"  "nmap <leader>c :!ctags --recurse --totals .<CR>
"
"  " Open the current file in the default program
"  nmap <leader>x :!xdg-open %<cr><cr>
"
"  " Artisan shortcuts
"  abbrev amod !php artisan make:model
"  abbrev amig !php artisan make:migration
"  abbrev ajob !php artisan make:job
"
"  " Switch between the last two files
"  nmap <leader><leader> <c-^>
"
"  " Create/edit file in the current directory
"  nmap :ed :edit %:p:h/
"
"  " Make vim act a bit more like readline
"  cnoremap <C-a>  <Home>
"  cnoremap <C-b>  <Left>
"  cnoremap <C-f>  <Right>
"  cnoremap <C-d>  <Delete>
"  cnoremap <M-b>  <S-Left>
"  cnoremap <M-f>  <S-Right>
"  cnoremap <M-d>  <S-right><Delete>
"  cnoremap <Esc>b <S-Left>
"  cnoremap <Esc>f <S-Right>
"  cnoremap <Esc>d <S-right><Delete>
"  cnoremap <C-g>  <C-c>
"
"  noremap Y y$
"
"  " Maintain the cursor position when yanking a visual selection
"  " http://ddrscott.github.io/blog/2016/yank-without-jank/
"  vnoremap y myy`y
"  vnoremap Y myY`y
"
"  " Reselect visual selection after indenting
"  vnoremap < <gv
"
"  " Reselect visual selection after de-indenting
"  vnoremap > >gv
"
"  " xnoremap K :move '<-2<CR>gv=gv
"  " xnoremap J :move '>+1<CR>gv=gv
"
"  " Clear search highlighting
"  nnoremap <silent> <leader>k :nohl<CR>
"
"  " Visually select last pasted text using same visual mode
"  nnoremap <expr> gp '`[' . strpart(getregtype(), 0, 1) . '`]'
"
"  " When text is wrapped, move by terminal rows, not lines, unless a count is
"  " provided
"  noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
"  noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')
"
"  cmap w!! %!sudo tee > /dev/null %
"
"  nmap c* *Ncgn
"  vmap c* *Ncgn
"
"  " In command mode (i.e. after pressing ':'), expand %% to the path of the
"  " current buffer. This allows you to easily open files from the same
"  " directory as the currently opened file.
"  cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'
"
"  " Allows you to easily replace the current word and all its occurrences.
"  nnoremap <Leader>rc :%s/\<<C-r><C-w>\>/
"  vnoremap <Leader>rc y:%s/<C-r>"/
"
"  " Allows you to easily change the current word and all occurrences to
"  " something else. The difference between this and the previous mapping is
"  " that the mapping below pre-fills the current word for you to change.
"  nnoremap <Leader>cc :%s/\<<C-r><C-w>\>/<C-r><C-w>
"  vnoremap <Leader>cc y:%s/<C-r>"/<C-r>"
"
"  " noremap A <nop>
"
"  imap ;; <Esc>A;<Esc>
"  imap ,, <Esc>A,<Esc>

