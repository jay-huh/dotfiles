" vim: set nospell foldmethod=marker foldlevel=0:

" ctags {{{
" Look for tags files in parent directories
"if has('path_extra')
"  setglobal tags-=./tags tags-=./tags; tags^=./tags;,./vendor-tags;
"endif
set tags+=tags
set tags+=/usr/include/tags

if filereadable( "/usr/include/cscope.out" )
  cs add /usr/include/cscope.out
endif
" ctags }}}
" Load cscope database {{{
" From Linux kernel documentation
if has("cscope")
  " Look for a 'cscope.out' file starting from the current directory,
  " going up to the root directory.
  let s:dirs = split(getcwd(), "/")
  while s:dirs != []
    let s:path = "/" . join(s:dirs, "/")
    if (filereadable(s:path . "/cscope.out"))
      execute "cs add " . s:path . "/cscope.out " . s:path . " -v"
      break
    endif
    let s:dirs = s:dirs[:-2]
  endwhile

  set csto=0  " Use cscope first, then ctags
  set cst     " Only search cscope
  set csverb  " Make cs verbose
endif
" Load cscope database }}}

" Keymaps for cscope {{{
  "      nmap `<C-\>`s :cs find s `<C-R>`=expand("`<cword>`")`<CR>``<CR>`
  "      nmap `<C-\>`g :cs find g `<C-R>`=expand("`<cword>`")`<CR>``<CR>`
  "      nmap `<C-\>`c :cs find c `<C-R>`=expand("`<cword>`")`<CR>``<CR>`
  "      nmap `<C-\>`t :cs find t `<C-R>`=expand("`<cword>`")`<CR>``<CR>`
  "      nmap `<C-\>`e :cs find e `<C-R>`=expand("`<cword>`")`<CR>``<CR>`
  "      nmap `<C-\>`f :cs find f `<C-R>`=expand("`<cfile>`")`<CR>``<CR>`
  "      nmap `<C-\>`i :cs find i ^`<C-R>`=expand("`<cfile>`")`<CR>`$`<CR>`
  "      nmap `<C-\>`d :cs find d `<C-R>`=expand("`<cword>`")`<CR>``<CR>`
  "      nmap <F6> :cnext <CR>
  "      nmap <F5> :cprev <CR>

  " Open a quickfix window for the following queries.
  "set cscopequickfix=s-,c-,d-,i-,t-,e-,g-

function JayCscope(type, query, ...)
  if a:0 > 0
    let cmd = 'vert scs find '.a:type.' '.a:query
  else
    let cmd = 'scs find '.a:type.' '.a:query
  endif
  execute cmd
  if &cscopequickfix != ""
    "botright copen
    quit  " close auto-opened window
    "`ctrl-x` for split
    "`ctrl-v` for vertical split
    "`ctrl-t` for new tab
    FzfPreviewQuickFix
  endif
endfunction

nmap <C-@>s :call JayCscope("s", "<C-R><C-W>")<CR>
nmap <C-@>g :call JayCscope("g", "<C-R><C-W>")<CR>
nmap <C-@>c :call JayCscope("c", "<C-R><C-W>")<CR>
nmap <C-@>t :call JayCscope("t", "<C-R><C-W>")<CR>
nmap <C-@>e :call JayCscope("e", "<C-R><C-W>")<CR>
nmap <C-@>f :call JayCscope("f", "<C-R>=expand("<cfile>")<CR>")<CR>
nmap <C-@>i :call JayCscope("i", "^<C-R>=expand("<cfile>")<CR>$")<CR>
nmap <C-@>d :call JayCscope("d", "<C-R><C-W>")<CR>

nmap <C-@><C-@>s :call JayCscope("s", "<C-R><C-W>", "v")<CR>
nmap <C-@><C-@>g :call JayCscope("g", "<C-R><C-W>", "v")<CR>
nmap <C-@><C-@>c :call JayCscope("c", "<C-R><C-W>", "v")<CR>
nmap <C-@><C-@>t :call JayCscope("t", "<C-R><C-W>", "v")<CR>
nmap <C-@><C-@>e :call JayCscope("e", "<C-R><C-W>", "v")<CR>
nmap <C-@><C-@>f :call JayCscope("f", "<C-R>=expand("<cfile>")<CR>", "v")<CR>
nmap <C-@><C-@>i :call JayCscope("i", "^<C-R>=expand("<cfile>")<CR>$", "v")<CR>
nmap <C-@><C-@>d :call JayCscope("d", "<C-R><C-W>", "v")<CR>

nmap <C-SPACE>s :call JayCscope("s", "<C-R><C-W>")<CR>
nmap <C-SPACE>g :call JayCscope("g", "<C-R><C-W>")<CR>
nmap <C-SPACE>c :call JayCscope("c", "<C-R><C-W>")<CR>
nmap <C-SPACE>t :call JayCscope("t", "<C-R><C-W>")<CR>
nmap <C-SPACE>e :call JayCscope("e", "<C-R><C-W>")<CR>
nmap <C-SPACE>f :call JayCscope("f", "<C-R>=expand("<cfile>")<CR>")<CR>
nmap <C-SPACE>i :call JayCscope("i", "^<C-R>=expand("<cfile>")<CR>$")<CR>
nmap <C-SPACE>d :call JayCscope("d", "<C-R><C-W>")<CR>

nmap <C-SPACE><C-SPACE>s :call JayCscope("s", "<C-R><C-W>", "v")<CR>
nmap <C-SPACE><C-SPACE>g :call JayCscope("g", "<C-R><C-W>", "v")<CR>
nmap <C-SPACE><C-SPACE>c :call JayCscope("c", "<C-R><C-W>", "v")<CR>
nmap <C-SPACE><C-SPACE>t :call JayCscope("t", "<C-R><C-W>", "v")<CR>
nmap <C-SPACE><C-SPACE>e :call JayCscope("e", "<C-R><C-W>", "v")<CR>
nmap <C-SPACE><C-SPACE>f :call JayCscope("f", "<C-R>=expand("<cfile>")<CR>", "v")<CR>
nmap <C-SPACE><C-SPACE>i :call JayCscope("i", "^<C-R>=expand("<cfile>")<CR>$", "v")<CR>
nmap <C-SPACE><C-SPACE>d :call JayCscope("d", "<C-R><C-W>", "v")<CR>

"Display the next error in the list that includes a file name.
"nnoremap <C-j> :cnext<CR>
"nnoremap <C-k> :cprev<CR>
" Keymaps for cscope }}}
