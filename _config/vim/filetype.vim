" vim: set nospell foldmethod=marker foldlevel=0:

" Filetype Overrides {{{
augroup filetypeoverrides
  autocmd!
  " Give JSON files JavaScript highlighting
  au BufRead,BufNewFile *.json set ft=json syntax=javascript
  " Format Mutt temp files as email
  au BufRead,BufNewFile *tmp/mutt* set ft=mail
augroup end
" Filetype Overrides }}}

autocmd filetype java setlocal foldmethod=syntax
autocmd filetype c,cc,cpp,h,hpp,json setlocal foldmethod=marker foldlevel=99
autocmd filetype c,cc,cpp,h,hpp,json setlocal foldmarker={,}
autocmd filetype python
      \ setlocal tabstop=4
      \ softtabstop=4
      \ shiftwidth=4
      \ expandtab
      \ autoindent
      \ foldmethod=indent
      \ fileformat=unix

autocmd filetype make setlocal noexpandtab

autocmd filetype text setlocal spell

" Use '//' instead of '/* */' comments
autocmd FileType php setlocal commentstring=//%s
" autocmd FileType html setlocal foldmethod=syntax
" Don't backup gopass temp files
au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile

" Google C++ Coding Stype Guide {{{
" Use spaces instead of tabs
set expandtab
" How many characters wide the tab character should be
set tabstop=2
" How many spaces to use instead of a tab
set shiftwidth=2
set softtabstop=2
" Width of text before breaking
set textwidth=80
set wrap

autocmd filetype c,cc,cpp,h,hpp setlocal equalprg=clang-format\ -style=google
if filereadable(".clang-format")
  autocmd filetype c,cc,cpp,h,hpp setlocal equalprg=clang-format
endif
" Google C++ Coding Stype Guide }}}
