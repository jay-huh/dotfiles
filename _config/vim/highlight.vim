" vim: set nospell foldmethod=marker foldlevel=0:

" ColorSchemeOverrides {{{
augroup ColorSchemeOverrides
  autocmd!
  autocmd VimEnter,ColorScheme * highlight SpellBad gui=undercurl
  autocmd VimEnter,ColorScheme * highlight SpellCap gui=undercurl

  " listchars such as TAB, trail...
  "autocmd ColorScheme * hi NonText ctermbg=NONE guibg=NONE

  autocmd VimEnter,ColorScheme * hi Normal ctermbg=NONE guibg=NONE
  autocmd VimEnter,ColorScheme * hi CursorLine cterm=NONE
  autocmd VimEnter,ColorScheme * hi CursorLineNr cterm=NONE ctermfg=214 guifg=orange
  " ALE and GitGutter Sign Column
  autocmd VimEnter,ColorScheme * hi _Error term=reverse ctermfg=red guifg=lightred
  autocmd VimEnter,ColorScheme * hi _Warning term=standout cterm=bold ctermfg=214 guifg=orange
  autocmd VimEnter,ColorScheme * hi def link ALEErrorSign _Error
  autocmd VimEnter,ColorScheme * hi def link ALEWarningSign _Warning

  "autocmd VimEnter,ColorScheme * highlight SignColumn ctermbg=NONE guibg=NONE
  "autocmd VimEnter,ColorScheme * highlight LineNr ctermfg=60 ctermbg=bg guibg=bg
  autocmd VimEnter,ColorScheme * hi Visual ctermfg=NONE cterm=reverse guifg=NONE gui=reverse
  autocmd VimEnter,ColorScheme * hi ALEError cterm=underline,reverse gui=undercurl,reverse
  autocmd VimEnter,ColorScheme * hi ALEWarning cterm=underline,reverse gui=undercurl,reverse

  " color for message of error and warning
  autocmd VimEnter,ColorScheme * hi ALEVirtualTextError ctermbg=52 ctermfg=darkred
  autocmd VimEnter,ColorScheme * hi ALEVirtualTextWarning ctermbg=18 ctermfg=blue

  autocmd VimEnter,ColorScheme * hi link CocErrorSign ALEErrorSign
  autocmd VimEnter,ColorScheme * hi link CocWarningSign ALEWarningSign
  autocmd VimEnter,ColorScheme * hi link CocErrorHighlight ALEError
  autocmd VimEnter,ColorScheme * hi link CocWarningHighlight ALEWarning

  autocmd VimEnter,ColorScheme * hi CurrentWord ctermfg=14 ctermbg=23 guifg=#00ffff guibg=#005f5f
  autocmd VimEnter,ColorScheme * hi CurrentWordTwins ctermbg=238 cterm=NONE guibg=#404040 gui=NONE
  autocmd VimEnter,ColorScheme * hi CocSearch ctermfg=lightcyan guifg=#00D0C0

  autocmd VimEnter,ColorScheme * hi MatchParen ctermbg=magenta guibg=darkmagenta guifg=yellow

  " public, protected, private kewords
  autocmd VimEnter,ColorScheme * hi cppAccess ctermfg=red guifg=lightred
  " class, namespace, struct
  autocmd VimEnter,ColorScheme * hi Structure ctermfg=green guifg=lightgreen
  " static, const
  autocmd VimEnter,ColorScheme * hi StorageClass ctermfg=214 guifg=orange
  "autocmd VimEnter,ColorScheme * hi Typedef
  "autocmd VimEnter,ColorScheme * hi Label
  autocmd VimEnter,ColorScheme * hi Type ctermfg=yellow guifg=#F9E3AF

  autocmd ColorScheme base16-* hi StatusLine ctermbg=234 guifg=darkgray
  autocmd ColorScheme base16-* hi Folded ctermfg=244 guifg=#808080
  autocmd ColorScheme base16-* hi Comment ctermfg=241 guifg=#626262
augroup end
" ColorSchemeOverrides }}}
" Popup window Colors {{{
"  " Popup window colors
"  highlight Pmenu term=none cterm=none ctermbg=234 ctermfg=68
"  highlight PmenuSel term=none cterm=none ctermbg=117 ctermfg=234
"  highlight PmenuSbar term=none cterm=none ctermbg=235
"  highlight Pmenuthumb term=none cterm=none ctermbg=68
" Popup windoe Colors }}}
" Termdebug {{{
" Termdebug highlight
if &background == 'light'
  hi default debugPC term=reverse ctermbg=lightblue guibg=lightblue
else
  "hi default debugPC term=reverse ctermbg=darkblue guibg=darkblue
  hi default debugPC term=reverse ctermbg=18 guibg=darkblue
endif
"hi default debugBreakpoint term=reverse ctermfg=black ctermbg=red guifg=black guibg=red
hi default debugBreakpoint term=reverse ctermfg=232 ctermbg=203 guifg=black guibg=red
" Termdebug }}}
"
"  " `WildMenu` is equal to `Pmenu`
"  highlight WildMenu cterm=none ctermbg=117 ctermfg=234
"  highlight StatusLine cterm=none ctermbg=234 ctermfg=68

" Terminal Mode
"autocmd TerminalWinOpen * setlocal notermguicolors
"autocmd TerminalWinOpen * hi Terminal ctermbg=none

" Indent line tab
"  hi SpecialKey ctermfg=130 ctermbg=none guibg=bg
"  hi Special ctermfg=196 ctermbg=none guibg=bg
