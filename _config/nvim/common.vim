" vim: set nospell foldmethod=marker foldlevel=0:

" zM: Close All Folds
" zR: Open All Folds

" Loading ~/.vimrc{{{
set runtimepath+=~/.vim,~/.vim/after
set packpath+=~/.vim
source ~/.config/vim/base.vim
source ~/.config/vim/keymap.vim
source ~/.config/vim/filetype.vim
source ~/.config/vim/auto-command.vim
source ~/.config/vim/tags-cscope.vim
" Loading ~/.vimrc }}}
