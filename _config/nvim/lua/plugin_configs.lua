require("plugin_configs.catppuccin")
require("plugin_configs.nvim-lspconfig")
require("plugin_configs.nvim-cmp")
require("plugin_configs.nvim-treesitter")
