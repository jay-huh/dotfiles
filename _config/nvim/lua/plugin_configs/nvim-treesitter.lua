vim.treesitter.language.register('c', 'cpp', 'lua', 'vim')  -- the someft filetype will use the python parser and queries.

local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
  return
end

--local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
--parser_config.zimbu = {
--  install_info = {
--    url = "~/projects/tree-sitter-zimbu", -- local path or git repo
--    files = {"src/parser.c"}, -- note that some parsers also require src/scanner.c or src/scanner.cc
--    -- optional entries:
--    branch = "main", -- default branch in case of git repo if different from master
--    generate_requires_npm = false, -- if stand-alone parser without npm dependencies
--    requires_generate_from_grammar = false, -- if folder contains pre-generated src/parser.c
--  },
--  filetype = "zu", -- if filetype does not match the parser name
--}

configs.setup {
  -- A list of parser names, or "all" (the five listed parsers should always be installed)
  ensure_installed = { "c", "cpp", "lua", "vim" },
  --ensure_installed = "maintained",
  sync_install = false, -- Install parsers synchronously (only applied to `ensure_installed`)
  auto_install = true, -- Automatically install missing parsers when entering buffer
  ignore_install = { "" },  -- List of parsers to ignore installing (for "all")
  indent = { enable = true, disable = { "yaml" } },
  highlight = {
    enable = true,
    --disable = { "" }, -- list of language that will be disabled
    disable = function(lang, buf)
        local max_filesize = 256 * 1024 -- 256 KB
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            return true
        end
    end,
    additional_vim_regex_highlighting = true,
  },
}

-- Define a function to count the number of lines in the current buffer
local function count_lines()
  local num_lines = vim.fn.line('$') -- Get the number of lines in the buffer
  --print('Number of lines in file: ' .. num_lines)
  if num_lines < 20000 then
    vim.opt.foldmethod  = 'expr'
    vim.opt.foldexpr    = 'nvim_treesitter#foldexpr()'
  else
    vim.opt.foldmethod  = 'manual'
  end
end

vim.api.nvim_create_autocmd({'BufEnter','BufAdd','BufNew','BufNewFile','BufWinEnter'}, {
  group = vim.api.nvim_create_augroup('TS_FOLD_WORKAROUND', {}),
  callback = function()
    count_lines()
  end
})
