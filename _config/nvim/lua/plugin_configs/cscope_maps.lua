-- load cscope maps
-- pass empty table to setup({}) for default options
local status_ok, cscope_maps = pcall(require, "cscope_maps")
if not status_ok then
  return
end

cscope_maps.setup({
  disable_maps = false, -- true disables my keymaps, only :Cscope will be loaded
  cscope = {
    db_file = "./cscope.out", -- location of cscope db file
  },
})
