-- Automatically install packer
-- You can also use the following command (with packer bootstrapped) to have packer
-- setup your configuration (or simply run updates) and close once all operations
-- are completed:

local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system({
    "git",
    "clone",
    "--depth=1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  })
  print("Installing packer. Close and reopen Neovim once done...")
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the `plugins.lua` file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use protected call so we don't error out on first Use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init({
  display = ({
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  }),
})

return packer.startup(function(use)
  use 'wbthomason/packer.nvim'
  use { "catppuccin/nvim", as = "catppuccin" }
  use {
    'nvim-treesitter/nvim-treesitter',
    run = function()
      local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
      ts_update()
    end,
  }
  use 'neovim/nvim-lspconfig' -- Configurations for Nvim LSP
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'SirVer/ultisnips'
  use 'quangnguyen30192/cmp-nvim-ultisnips'
  --use 'L3MON4D3/LuaSnip'
  --use 'saadparwaiz1/cmp_luasnip'
  --use 'ms-jpq/coq_nvim'
  --use 'ms-jpq/coq.artifacts'

  --use {
  --  "williamboman/mason.nvim",
  --  run = ":MasonUpdate" -- :MasonUpdate updates registry contents
  --}
  use 'jiangmiao/auto-pairs'
  use 'dhananjaylatkar/cscope_maps.nvim' -- cscope keymaps
  use 'folke/which-key.nvim' -- optional

  use 'itchyny/lightline.vim'
  use 'mengelbrecht/lightline-bufferline'
  vim.cmd([[
  "" {{{ lightline
  ""set background=dark
  ""let g:lightline = {
  "      \ 'colorscheme': 'wombat',
  "      \ 'active': {
  "      \   'left': [ [ 'mode', 'paste' ],
  "      \             [ 'gitbranch', 'cocstatus', 'readonly', 'relativepath', 'modified' ] ]
  "      \ },
  "      \ 'component_function': {
  "      \   'gitbranch': 'FugitiveHead',
  "      \   'cocstatus': 'coc#status'
  "      \ },
  "      \ 'tabline': {
  "      \   'left': [ ['tabs'], ['buffers'] ],
  "      \   'right': [ ['close'] ]
  "      \ },
  "      \ 'tabline_separator': { 'left': '', 'right': '' },
  "      \ 'tabline_subseparator': { 'left': '', 'right': '' },
  "      \ 'tab': {
  "      \   'active': [ 'tabnum' ],
  "      \   'inactive': [ 'tabnum' ]
  "      \ },
  "      \ 'component_expand': {
  "      \   'buffers': 'lightline#bufferline#buffers',
  "      \   'tabs': 'lightline#tabs'
  "      \ },
  "      \ 'component_type': {
  "      \   'buffers': 'tabsel'
  "      \ },
  "      \ }
  "" }}} lightline
  ]])
  use 'dominikduda/vim_current_word'
  vim.cmd([[
  let g:vim_current_word#enabled = 1
  " Twins of word under cursor:
  let g:vim_current_word#highlight_twins = 1
  " The word under cursor:
  let g:vim_current_word#highlight_current_word = 1
  let g:vim_current_word#highlight_only_in_focused_window = 1
  let g:vim_current_word#excluded_filetypes = ['ruby']
    ]])
  use 'tpope/vim-fugitive'
  use 'airblade/vim-gitgutter'
  use {
      'junegunn/fzf.vim',
      requires = { 'junegunn/fzf', run = ':call fzf#install()' }
  }
  use 'christoomey/vim-tmux-navigator'
  --use { "alexghergh/nvim-tmux-navigation" }
  use {
    "windwp/nvim-autopairs",
    -- require("nvim-autopairs").setup {}
    config = function() require("nvim-autopairs").setup {} end
  }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require('packer').sync()
  end
end)

--local ok, packer = pcall(require, "packer")
--if not ok then
--    print("packer.nvim not installed. No plugins will get installed.")
--    return
--end
--
--packer.init({
--    display = {
--        open_fn = function()
--            return require("packer.util").float({ border = "rounded" })
--        end,
--    },
--})
