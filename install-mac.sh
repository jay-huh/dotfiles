#!/bin/sh

set -e

echo "Installing brew"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

eval "$(/opt/homebrew/bin/brew shellenv)"

# TODO: amphetamine is not installed using brew

brew bundle install --file=./Brewfile-lite
brew bundle check --file=./Brewfile-lite
brew bundle cleanup --force

brew services start borders

# set screenshot file format to jpg
defaults write com.apple.screencapture type jpg && killall SystemUIServer
# Enable vim key repeating
#defaults write com.microsoft.VSCode ApplePressAndHoldEnabled -bool false
# Enable key repeating on system-wide
defaults write -g ApplePressAndHoldEnabled -bool false

# System Settings -> Keyboard -> Key repeat rate(fatest - 1)
# System Settings -> Keyboard -> Delay until repeat(shrtest - 2)
# default: 25
#defaults write -g InitialKeyRepeat -int 20
# default: 6
#defaults write -g KeyRepeat -int 4

# CASK
#brew install --cask google-chrome

# Password manager
#brew install --cask bitwarden
#brew install --cask 1password

#brew install --cask scroll-reverser

# Development CLI Tools
#brew install bear
#brew install --cask docker
# Colorized manpages => oh-my-zsh/colored-man-pages
#brew install most

## install JAVA
#brew install java
##For the system Java wrappers to find this JDK, symlink it with
#sudo ln -sfn /opt/homebrew/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk

# Optional Cask Apps
#brew install --cask spark

######## Pakages for WICED and `ceedling`
## Install `arm-none-eabi` packages
#brew install npm
#npm install --global xpm@latest
#xpm install --global @xpack-dev-tools/arm-none-eabi-gcc@latest --verbose
#
## Located path: `/Applications/ARM/arm-none-eabi`
##brew install gcc-arm-embedded
#
## install gem and ceedling
#sudo gem install ceedling -v 0.30.0

./install-zsh.sh

## install hammerspoon config file using stow
#stow -v hammerspoon -t ${HOME}/.hammerspoon
