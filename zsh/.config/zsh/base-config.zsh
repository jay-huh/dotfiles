source ${HOME}/.commonrc.sh

#zmodload zsh/zprof

## vcs_info
#setopt prompt_subst
#autoload -Uz vcs_info
#
#precmd_vcs_info() { vcs_info }
#precmd_functions+=( precmd_vcs_info )
#
#zstyle ':vcs_info:*' enable git
#zstyle ':vcs_info:*' check-for-changes true
#zstyle ':vcs_info:*' get-revision true
#zstyle ':vcs_info:*' stagedstr " S"  # ' ✚'
#zstyle ':vcs_info:*' unstagedstr " U"  # " *" ' ±'
#
## action: %a rebase, merge, cherry-pick
#zstyle ':vcs_info:*' formats '%F{yellow}(%b%u%c)%f'
#zstyle ':vcs_info:*' actionformats '%F{yellow}(%b | %a%u%c)%f'

## Use modern completion system
autoload -Uz compinit
compinit

#zstyle ':completion:*' auto-description 'specify: %d'
## enable approximate matches for completion
#zstyle ':completion:::::' completer _expand _complete _ignored _approximate
##zstyle ':completion:*' completer _expand _complete _correct _approximate
###zstyle ':completion:*' format 'Completing %d'
## Show group and formatting
##zstyle ':completion:*' format '%F{magenta}-- %d --%f'
#zstyle ':completion:*' group-name '' # group results by category
#
#zstyle ':completion:*' menu select=long
zstyle ':completion:*' menu select=2
#zstyle ':completion:*' yes select
##eval "$(dircolors -b ~/.dircolors)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
##zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
#zstyle ':completion:*' matcher-list ':{a-zA-Z}={A-Za-z} r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
#zstyle ':completion:*' use-compctl false
#zstyle ':completion:*' verbose true
#
#zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
#zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# use the vi navigation keys in menu completion
zmodload zsh/complist
_comp_options+=(globdots)		# Include hidden files.
zle_highlight=('paste:none')

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

bindkey '^p' history-search-backward
bindkey '^n' history-search-forward

####################################################################

# If you come from bash you might have to change your $PATH.
#export PATH=$HOME/bin:/usr/local/bin:$PATH
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi

#echo "######## PATH: $PATH"
# set PATH so it includes user's private bin if it exists
if [[ -d "$HOME/.local/bin" ]] ; then
  PATH="$HOME/.local/bin:$PATH"
fi

# User configuration

# Escape square brackets by default
unsetopt nomatch

# select first item
#setopt MENU_COMPLETE
#setopt GLOB_DOTS
#setopt EXTENDED_GLOB
#setopt INTERACTIVE_COMMENTS

# completion behave like Bash
unsetopt MENU_COMPLETE

# append to the history file, don't overwrite it
setopt append_history
setopt share_history # share history between different instances of the shell
setopt hist_ignore_all_dups # remove older duplicate entries from history
setopt hist_reduce_blanks # remove superfluous blanks from history items
setopt inc_append_history # save history entries as soon as they are entered

setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.

setopt auto_cd # cd by typing directory name if it's not a command
#setopt correct_all # autocorrect commands

setopt auto_list # automatically list choices on ambiguous completion
setopt auto_menu # automatically use menu completion
setopt always_to_end # move cursor to end if word had one match

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
#shopt -s checkwinsize
# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

bindkey -v

export HISTFILE=${XDG_CONFIG_HOME:-$HOME/.config}/zsh/history
