#PROMPT='%F{green}%n%f@%F{033}%m%f %F{blue}%~%f $vcs_info_msg_0_'
#
#precmd() { print "" }
## add newline
#if [[ -z $SEGMENT_SEPARATOR ]] then ;
#  SEGMENT_SEPARATOR="$"
#fi
#PS1=${PS1}$'\n''%F{blue}%\\$%f '
#zle_highlight=(default:bold) # command line bold text

test -x $(which starship) 2> /dev/null && eval "$(starship init zsh)"
