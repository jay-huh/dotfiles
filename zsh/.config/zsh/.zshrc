# Created by Zap installer

if [[ ! -d "${HOME}/.local/share/zap" ]] ; then
  mv ${HOME}/.config/zsh/.zshrc ${HOME}/.config/zsh/.zshrc-init
  zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.zsh) --branch release-v1
fi

[ -f "$HOME/.local/share/zap/zap.zsh" ] && source "$HOME/.local/share/zap/zap.zsh"
plug "zsh-users/zsh-autosuggestions"
#plug "zap-zsh/supercharge"
#plug "zap-zsh/zap-prompt"
plug "zsh-users/zsh-syntax-highlighting"

ZSH_CONFIG_DIR=${XDG_CONFIG_HOME:-$HOME/.config}/zsh
echo "Append post script to ${ZSH_CONFIG_DIR}/.zshrc"
echo -e "\n\nsource ${ZSH_CONFIG_DIR}/post-zshrc" >> ${ZSH_CONFIG_DIR}/.zshrc
