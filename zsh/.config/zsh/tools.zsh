
export NVM_DIR=${HOME}/.nvm

[ -s ${NVM_DIR}/nvm.sh ] && \. ${NVM_DIR}/nvm.sh  # This loads nvm
[ -s ${NVM_DIR}/bash_completion ] && \. ${NVM_DIR}/bash_completion  # This loads nvm bash_completion

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
test -e ${HOME}/.iterm2_shell_integration.zsh && source ${HOME}/.iterm2_shell_integration.zsh

#ZSH_TMUX_TERM=xterm-256color

test -x $(which zoxide) 2> /dev/null && eval "$(zoxide init zsh)"

# Base16 Shell
if [[ ! -d "${HOME}/.config/base16-shell" ]] ; then
  git clone https://github.com/tinted-theming/base16-shell.git $HOME/.config/base16-shell
fi

BASE16_SHELL_PATH="$HOME/.config/base16-shell"
[ -n "$PS1" ] && \
  [ -s "$BASE16_SHELL_PATH/profile_helper.sh" ] && \
    source "$BASE16_SHELL_PATH/profile_helper.sh"
