#include <stdio.h>
#include <inttypes.h>

int main(int argc, char *argv[])
{
  float f = 12.8888888888f;
  int result;

  result = (int)(f * (float)100);

  printf("%f x 100 = %d ==> %0.2f\n", f, result, result / 100.0);

  uint16_t tmp;
  for(int i = 0 ; i < 0xFFFF ; i += 0x100) {
    tmp = (uint16_t)(i * 0.815f);
    printf("%d ==> %u\n", i, tmp);

  }
  return 0;
}
