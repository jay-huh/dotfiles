#ifndef OFFICE_VISITOR_H
#define OFFICE_VISITOR_H

#include "visitor.h"

#include "office.h"

class SeoulOfficeVisitor : public Visitor<SeoulOffice> {
 private:
 public:
  SeoulOfficeVisitor() = default;
  virtual ~SeoulOfficeVisitor() override = default;

  virtual int Visit(SeoulOffice& office) override {
    std::cout << "SeoulOfficeVisitor @ " << this << " | visit ";
    office.Print();
    return 0;
  }
};

class BusanOfficeVisitor : public Visitor<BusanOffice> {
 private:
 public:
  BusanOfficeVisitor() = default;
  virtual ~BusanOfficeVisitor() override = default;

  virtual int Visit(BusanOffice& office) override {
    std::cout << "BusanOfficeVisitor @ " << this << " | visit ";
    office.Print();
    return 0;
  }
};

#endif /* OFFICE_VISITOR_H */
