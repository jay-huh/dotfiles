#include "office_visitor.h"
#include "office.h"

#include <iostream>
#include <queue>

#include "test_device.h"
#include "device_handler.h"

#include "interface.h"
#include "interface_handler.h"

void test_visitor() {
  Office office("Head");
  Office *pOffice_1 = new SeoulOffice();
  Office *pOffice_2 = new BusanOffice();
  SeoulOfficeVisitor visitor;
  BusanOfficeVisitor visitor_1;

  // Visit Head office
  office.Accept(visitor);
  office.Accept(visitor_1);

  // Visit SeoulOffice
  pOffice_1->Accept(visitor);
  pOffice_1->Accept(visitor_1);

  // Visit BusanOffice
  pOffice_2->Accept(visitor);
  pOffice_2->Accept(visitor_1);

  delete pOffice_1;
  delete pOffice_2;
}

void test_interface() {
  Interface *interface = new BfmInterface();
  Handler *handler = nullptr;

  handler = new HostIfWriteHandler(dynamic_cast<BfmInterface*>(interface));
  interface->Dispatch(*handler);
  delete handler;

  handler = new HostIfReadHandler(dynamic_cast<BfmInterface*>(interface));
  interface->Dispatch(*handler);
  delete handler;

  delete interface;
}

void test_device() {
  Device *device = new TestDevice("T_1", 100);
  Handler *handler = nullptr;
  std::queue<Handler*> handlers;

  std::cout << "Generate handlers..." << std::endl;
  for (int i = 0; i < 10; i++) {
    if (i % 2 == 0) {
      handler = new ReadHandler(device);
    } else {
      handler = new TestWriteHandler(dynamic_cast<TestDevice*>(device));
    }
    std::cout << handler << std::endl;
    handlers.push(handler);
  }

  std::cout << "Dispatch handles..." << std::endl;
  while (handlers.empty() == false) {
    handler = handlers.front();
    handlers.pop();
    std::cout << "Handler @ " << handler << std::endl;
    device->Dispatch(*handler);
    std::cout << std::endl;
    delete handler;
  }
  delete device;
}

void test_device_simple() {
  Device *device = new TestDevice("T_1", 100);
  Handler *handler = nullptr;

  handler = new ReadHandler(device);
  device->Dispatch(*handler);
  delete handler;

  handler = new TestWriteHandler(dynamic_cast<TestDevice*>(device));
  device->Dispatch(*handler);
  delete handler;

  delete device;
}

int main(int argc, char *argv[]) {
  test_visitor();
  test_interface();
  test_device();
  test_device_simple();

  return 0;
}
