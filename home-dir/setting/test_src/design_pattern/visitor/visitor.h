#ifndef VISITOR_H
#define VISITOR_H

template <typename T>
class Visitor {
 private:
 public:
  Visitor() = default;
  virtual ~Visitor() = default;

  virtual int Visit(T&) = 0;
};

#endif /* VISITOR_H */
