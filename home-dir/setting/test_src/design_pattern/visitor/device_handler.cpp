#include "device_handler.h"

#include <iostream>

#include "device.h"

int ReadHandler::Handle(void *) {
  std::cout << "ReadHandler... ";
  device->Print();
  return 0;
}

int WriteHandler::Handle(void *) {
  std::cout << "WriteHandler... ";
  device->Print();
  return 0;
}

#include "test_device.h"

int TestReadHandler::Handle(void *) {
  std::cout << "TestReadHandler... ";
  device->Print();
  return 0;
}

int TestWriteHandler::Handle(void *) {
  std::cout << "TestWriteHandler... ";
  device->Print();
  return 0;
}
