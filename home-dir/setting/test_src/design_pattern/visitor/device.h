#ifndef DEVICE_H
#define DEVICE_H

#include "interface.h"

#include <string>

class Device : public Interface {
 private:
  std::string name_;

 protected:
 public:
  explicit Device(const std::string& name) : name_(name) {};
  virtual ~Device() = default;

  std::string& Name() { return name_; }

  virtual void Print(void) = 0;
};

#endif /* DEVICE_H */
