#ifndef INTERFACE_H
#define INTERFACE_H

#include <cstdint>
#include <cstddef>  // size_t

#include "handler.h"

class Interface {
 private:
 public:
  Interface() = default;
  virtual ~Interface() = default;

  virtual int Dispatch(Handler &handler, void *arg = nullptr) {
    return handler.Handle(arg);
  };
  virtual int Read(uint8_t *buffer, size_t length) = 0;
  virtual int Write(uint8_t *buffer, size_t length) = 0;
};

#include <iostream>
class BfmInterface : public Interface {
 private:
 public:
  BfmInterface() = default;
  virtual ~BfmInterface() override = default;

  virtual int Read(uint8_t *buffer, size_t length) override {
    std::cout << "BfmInterface::Read()" << std::endl;
    return 0;
  };
  virtual int Write(uint8_t *buffer, size_t length) override {
    std::cout << "BfmInterface::Write()" << std::endl;
    return 0;
  };

  void Print(void) { std::cout << "BfmInterface" << std::endl; }
};

#endif /* INTERFACE_H */
