#ifndef HANDLER_H
#define HANDLER_H

class Handler {
 private:
 protected:
 public:
  Handler() = default;
  virtual ~Handler() = default;

  virtual int Handle(void* = nullptr) = 0;
};

#endif /* HANDLER_H */
