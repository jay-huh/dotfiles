#ifndef TEST_DEVICE_H
#define TEST_DEVICE_H

#include "device.h"

class TestDevice : public Device {
 private:
  int id;

 public:
  TestDevice(const std::string& name, const int id) : Device(name), id(id) {};
  virtual ~TestDevice() override = default;

  virtual int Read(uint8_t *buffer, size_t length) override {
    std::cout << "TestDevice::Read()" << std::endl;
    return 0;
  };
  virtual int Write(uint8_t *buffer, size_t length) override {
    std::cout << "TestDevice::Write()" << std::endl;
    return 0;
  };
  virtual void Print(void) override;
};

#endif /* TEST_DEVICE_H */
