#ifndef INTERFACE_HANDLER_H
#define INTERFACE_HANDLER_H

#include "handler.h"

class BfmInterface;

class HostIfReadHandler : public Handler {
 private:
  BfmInterface* const interface;

 public:
  explicit HostIfReadHandler(BfmInterface* const interface)
      : interface(interface) {};
  virtual ~HostIfReadHandler() override = default;

  virtual int Handle(void* arg = nullptr) override;
};

class HostIfWriteHandler : public Handler {
 private:
  BfmInterface* const interface;

 public:
  explicit HostIfWriteHandler(BfmInterface* const interface)
      : interface(interface) {};
  virtual ~HostIfWriteHandler() override = default;

  virtual int Handle(void* arg = nullptr) override;
};
#endif /* INTERFACE_HANDLER_H */
