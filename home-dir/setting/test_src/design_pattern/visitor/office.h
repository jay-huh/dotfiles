#ifndef OFFICE_H
#define OFFICE_H

#include "accepter.h"

#include <iostream>

class Office : public Accepter {
 private:
  const char* location;

 public:
  explicit Office(const char* location) : location(location) {};
  virtual ~Office() override = default;

  const char* Location() { return location; }
  void Print() {
    std::cout << "Location: " << Location() << std::endl;
  }
};

class SeoulOffice : public Office {
 private:
 public:
  SeoulOffice() : Office("Seoul") {};
  virtual ~SeoulOffice() override = default;
};

class BusanOffice : public Office {
 private:
 public:
  BusanOffice() : Office("Busan") {};
  virtual ~BusanOffice() override = default;
};
#endif /* OFFICE_H */
