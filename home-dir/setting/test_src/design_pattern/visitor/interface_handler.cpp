#include "interface_handler.h"

#include <iostream>

#include "interface.h"

int HostIfReadHandler::Handle(void *) {
  std::cout << "HostIfReadHandler... Dispatched by ";
  interface->Print();
  interface->Write(nullptr, 0);
  interface->Read(nullptr, 0);
  return 0;
}
int HostIfWriteHandler::Handle(void *) {
  std::cout << "HostIfReadHandler... Dispatched by ";
  interface->Print();
  interface->Write(nullptr, 0);
  interface->Write(nullptr, 0);
  return 0;
}
