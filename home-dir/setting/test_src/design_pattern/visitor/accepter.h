#ifndef ACCEPTER_H
#define ACCEPTER_H

#include "visitor.h"

class Accepter {
 private:
 public:
  Accepter() = default;
  virtual ~Accepter() = default;

  template <typename T>
  void Accept(Visitor<T> &visitor) {
    visitor.Visit(reinterpret_cast<T&>(*this));
  };
};

#endif /* ACCEPTER_H */
