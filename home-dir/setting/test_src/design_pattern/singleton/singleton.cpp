#include "singleton.h"

#include <cstdlib>
#include <iostream>
#include <vector>
#include <thread>

// compile: clang++ -Wall -o singleton singleton.cpp -pthread -std=c++11

//Singleton Singleton::instance;

Singleton *Singleton::pInstance_ = nullptr;
std::mutex Singleton::mtx_;

int main(int argc, char *argv[]) {
  std::vector<std::thread> threads;

  for (int i = 0; i < 10; i++) {
    threads.emplace_back(std::thread([=]() {
      Singleton::GetPtrInstance()->SetName("new_name_" + std::to_string(i));
      Singleton::DestroyInstance();
    }));
  }

  for (auto &t : threads) {
    if (t.joinable()) {
      t.join();
    }
  }

#ifdef __STATIC_SINGLETON__
  std::cout << "Start!!!!" << std::endl;

  Singleton &instance = Singleton::GetStaticInstance();

  Singleton::GetStaticInstance().Print();
  Singleton::GetStaticInstance().SetName("Jay's Test");
  instance.Print();

  instance.SetName("Final");
  Singleton::GetStaticInstance().Print();
#endif

  return EXIT_SUCCESS;
}
