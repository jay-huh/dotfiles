#ifndef SINGLETON_H
#define SINGLETON_H

#include <iostream>
#include <mutex>

using namespace std;

//#define __STATIC_SINGLETON__

class Singleton {
#ifdef __STATIC_SINGLETON__
 private:
 public:
  static Singleton& GetStaticInstance_() {
    static Singleton static_instance_("Static_singleton");
    return static_instance_;
  };
#endif

 private:
  static Singleton* pInstance_;
  static std::mutex mtx_;

 public:
  static Singleton* GetPtrInstance() { return CreateInstance(); };

  static Singleton* CreateInstance(
      const std::string& name = "Dynamic_singleton") {
    std::lock_guard<std::mutex> lock(mtx_);

    if (pInstance_ == nullptr) {
      pInstance_ = new Singleton(name);
    } else {
      std::cout << "Reuse!!" << " name: " << name << std::endl;
    }
    return pInstance_;
  };
  static void DestroyInstance() {
    std::lock_guard<std::mutex> lock(mtx_);

    if (pInstance_ != nullptr) {
      delete pInstance_;
      pInstance_ = nullptr;
    } else {
      std::cout << "Pass" << std::endl;
    }
  };

 private:
  Singleton(const std::string& name = "Singleton") : name(name) {
    std::cout << "Created!!" << " name: " << this->name << " @" << this
              << std::endl;
  };

  std::string name;

 public:
  virtual ~Singleton() {
    static int count = 0;
    count++;
    std::cout << count << " | " << "Destroyed!!" << " name: " << this->name
              << " @" << this << std::endl;
  };

  // Copy and Move constructor
  Singleton(const Singleton&) = delete;
  Singleton(const Singleton&&) = delete;
  // Copy and Move assignment operator
  Singleton& operator=(const Singleton&) = delete;
  Singleton& operator=(const Singleton&&) = delete;

  void SetName(const std::string& name) { this->name = name; }
  void Print() { std::cout << "Name: " << name << " @" << this << std::endl; }
};

#endif /* SINGLETON_H */

