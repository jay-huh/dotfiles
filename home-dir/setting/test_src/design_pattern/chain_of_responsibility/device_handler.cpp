#include "device_handler.h"

#include <iostream>

#include "test_device.h"

int ReadHandler::Handle(void *) {
  std::cout << "ReadHandler... ";
  device->Print();
  device->Write(nullptr, nullptr, 0, 0);
  device->Read(nullptr, nullptr, 0, 0);
  return 0;
}

int WriteHandler::Handle(void *) {
  std::cout << "WriteHandler... ";
  device->Print();
  device->Write(nullptr, nullptr, 0, 0);
  device->Write(nullptr, nullptr, 0, 0);
  return 0;
}

#include "test_device.h"

int TestReadHandler::Handle(void *) {
  std::cout << "TestReadHandler... ";
  device->Print();
  device->Write(nullptr, nullptr, 0, 0);
  device->Read(nullptr, nullptr, 0, 0);
  return 0;
}

int TestWriteHandler::Handle(void *) {
  std::cout << "TestWriteHandler... ";
  device->Print();
  device->Write(nullptr, nullptr, 0, 0);
  device->Write(nullptr, nullptr, 0, 0);
  return 0;
}
