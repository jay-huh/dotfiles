#ifndef CHAIN_H
#define CHAIN_H

template <typename T>
class Chain {
 private:
 protected:
  T* successor_;

 public:
  Chain() : successor_(nullptr) {};
  virtual ~Chain() = default;

  T* SetNext(T* successor) {
    successor_ = successor;
    return successor;
  }
  T* Next() { return successor_; }
};

#endif /* CHAIN_H */
