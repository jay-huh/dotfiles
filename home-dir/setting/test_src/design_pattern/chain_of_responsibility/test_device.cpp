#include "test_device.h"

#include <iostream>

int TestDevice::Read(void *addr, void *src, size_t length, size_t item_size) {
  std::cout << "[Read() " << Name() << " " << Id() << "]";
  if (Next()) {
    std::cout << " -> ";
    Next()->Read(addr, src, length, item_size);
  } else {
    std::cout << std::endl;
  }
  return 0;
}

int TestDevice::Write(void *addr, void *src, size_t length, size_t item_size) {
  std::cout << "[Write() " << Name() << " " << Id() << "]";
  if (Next()) {
    std::cout << " -> ";
    Next()->Write(addr, src, length, item_size);
  } else {
    std::cout << std::endl;
  }
  return 0;
}

void TestDevice::Print(void) {
  std::cout << "TestDevice: name = " << Name() << " id = " << Id() << std::endl;
}
