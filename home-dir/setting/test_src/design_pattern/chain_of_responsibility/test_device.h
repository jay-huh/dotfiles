#ifndef TEST_DEVICE_H
#define TEST_DEVICE_H

#include "device.h"

class TestDevice : public Device {
 private:
  int id_;

 public:
  explicit TestDevice(const int id = 0) : Device("TestDevice"), id_(id) {};
  virtual ~TestDevice() override = default;

  int Id() { return id_; };

  int Read(void *addr, void *src, size_t length, size_t item_size) override;
  int Write(void *addr, void *src, size_t length, size_t item_size) override;

  virtual void Print(void) override;
};
#endif /* TEST_DEVICE_H */
