#ifndef DEVICE_H
#define DEVICE_H

#include "chain.h"
#include "handler.h"

#include <string>

class Device : public Chain<Device> {
 private:
  std::string name_;

 protected:
 public:
  explicit Device(const std::string &name) : name_(name) {};
  virtual ~Device() = default;

  std::string &Name() { return name_; }

  virtual int Read(void *addr, void *src, size_t length, size_t item_size) = 0;
  virtual int Write(void *addr, void *src, size_t length, size_t item_size) = 0;
#if 0
  size_t fread(void *buffer, size_t size, size_t count, FILE *stream);
  size_t fwrite(const void *buffer, size_t size, size_t count, FILE *stream);

  ssize_t read(int fides, void *buf, size_t nbytes);
  ssize_t write(int fildes, const void *buf, size_t nbytes);
#endif
  virtual int Dispatch(Handler &handler, void *arg = nullptr) {
    return handler.Handle(arg);
  };

  virtual void Print(void) = 0;
};

#endif /* DEVICE_H */
