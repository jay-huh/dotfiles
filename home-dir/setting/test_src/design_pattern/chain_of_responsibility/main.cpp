#include "test_device.h"

#include <iostream>
#include <vector>

#include "device_handler.h"
#if 0
class Handler_A : public DeviceHandler
{
private:

public:
  Handler_A() = default;
  virtual ~Handler_A() = default;

  virtual int Handle(Device &device) override {
    std::cout << "Handler_A: Handle()" << std::endl;
    //device.Read(nullptr, nullptr, 0,0);
    //std::cout << std::endl;
    //device.Write(nullptr, nullptr, 0,0);
  }
};
#endif

int main(int argc, char *argv[])
{
  std::vector<Device*>device_list;
  Device *device = nullptr;
  Device *pre_device = nullptr;

  std::vector<Device*>::iterator iter;

  for(int i = 0 ; i < 10 ; i++) {
    device = new TestDevice(i);
    device_list.emplace_back(device);
    if(pre_device) {
      pre_device->SetNext(device);
    }
    pre_device = device;
  }

  //Handler *handler = new ReadHandler(device);
  Handler *handler = new TestReadHandler(dynamic_cast<TestDevice*>(device));

  for(iter = device_list.begin(); iter != device_list.end(); ++iter){
    std::cout << ">>> Dispatch handler to device_" << dynamic_cast<TestDevice*>(*iter)->Id() << std::endl;
    (*iter)->Dispatch(*handler);
    std::cout << std::endl << ">>> Finish" << std::endl << std::endl;
  }

  for(iter = device_list.begin(); iter != device_list.end(); ++iter){
    (*iter)->Read(nullptr, nullptr, 0, 0);
    (*iter)->Write(nullptr, nullptr, 0, 0);
  }

  while(device_list.empty() == false) {
    device = device_list.back();
    device_list.pop_back();
    delete device;
  }
  std::cout << "# of list: " << device_list.size() << std::endl;

  return 0;
}
