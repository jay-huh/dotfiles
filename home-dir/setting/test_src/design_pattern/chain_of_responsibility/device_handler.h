#ifndef DEVICE_HANDLER_H
#define DEVICE_HANDLER_H

#include "handler.h"

class Device;

class ReadHandler : public Handler {
 private:
  Device* const device;

 public:
  explicit ReadHandler(Device* const device) : device(device) {};
  virtual ~ReadHandler() override = default;

  virtual int Handle(void* arg = nullptr) override;
};

class WriteHandler : public Handler {
 private:
  Device* const device;

 public:
  explicit WriteHandler(Device* const device) : device(device) {};
  virtual ~WriteHandler() override = default;

  virtual int Handle(void* arg = nullptr) override;
};

class TestDevice;

class TestReadHandler : public Handler {
 private:
  TestDevice* const device;

 public:
  explicit TestReadHandler(TestDevice* const device) : device(device) {};
  virtual ~TestReadHandler() override = default;

  virtual int Handle(void* arg = nullptr) override;
};

class TestWriteHandler : public Handler {
 private:
  TestDevice* const device;

 public:
  explicit TestWriteHandler(TestDevice* const device) : device(device) {};
  virtual ~TestWriteHandler() override = default;

  virtual int Handle(void* arg = nullptr) override;
};

#endif /* DEVICE_HANDLER_H */
