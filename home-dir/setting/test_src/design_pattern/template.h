#ifndef TEMPLATE_H
#define TEMPLATE_H

class Template {
 private:
 public:
  Template () = default;
  virtual ~Template () = default;
  // Copy and Move constructor
  Template(const Template &) = delete;
  Template(const Template &&) = delete;
  // Copy and Move assignment operator
  Template &operator=(const Template &) = delete;
  Template &operator=(const Template &&) = delete;
};

#endif /* TEMPLATE_H */
