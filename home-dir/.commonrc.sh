#!/bin/sh

echo "START $0"

export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-$HOME/.config}

if [ -d "/opt/homebrew/bin" ] ; then
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
#HISTCONTROL=ignoredups:erasedups
HISTCONTROL=ignoreboth:erasedups

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=5000
SAVEHIST=${HISTSIZE}
HISTFILESIZE=${HISTSIZE}
HISTDUP=erase
HISTIGNORE='&:ls:ll:la:cd:exit:clear:history:fg:man'

# zsh
HISTORY_IGNORE="([bf]g|l|l[alsh,] *|vi[m,] *|nvim *|less *|man *|cd [~-]|cd ..|pwd|exit|make|sudo reboot|history)"

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

if [ -z "$GREP_OPTIONS" ]; then
	for PATTERN in .cvs .git .svn .hg;
	do
		GREP_OPTIONS+="--exclude-dir=$PATTERN "
	done
	for PATTERN in tags cscope\.files cscope\.out;
	do
		GREP_OPTIONS+="--exclude=$PATTERN "
		AG_OPTIONS+="--ignore \\\"$PATTERN\\\" "
	done

	GREP_OPTIONS+='--binary-files=without-match '
	#export GREP_OPTIONS+='--binary-files=without-match'
	#export GREP_COLOR='1:32'
else
	echo "GREP_OPTIONS has been defined... $GREP_OPTIONS"
fi

alias grep="grep --color=auto $GREP_OPTIONS"
alias fgrep="fgrep --color=auto $GREP_OPTIONS"
alias egrep="egrep --color=auto $GREP_OPTIONS"
echo "GREP_OPTIONS: $GREP_OPTIONS"
## Summary for args to less:
# less(1)
#   -M (-M or --LONG-PROMPT) Prompt very verbosely
#   -I (-I or --IGNORE-CASE) Searches with '/' ignore case
#   -R (-R or --RAW-CONTROL-CHARS) For handling ANSI colors
#   -F (-F or --quit-if-one-screen) Auto exit if <1 screen
#   -X (-X or --no-init) Disable termcap init & deinit
alias ag="ag --smart-case --pager=\"less -MIRFX\" ${AG_OPTIONS}"
unset GREP_OPTIONS AG_OPTIONS

alias rg="rg --hidden"

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
if [[ $OSTYPE == "darwin"* ]] ; then
  alias ls='gls --color=auto'
  alias dir='gdir --color=auto'
else
  alias ls='ls --color=auto'
fi
test -r `which lsd` &> /dev/null && {
  alias ls='lsd'
  alias lt='ls --tree'
}

alias l='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias ll='ls -alF'

test -r `which fdfind` &> /dev/null && {
  alias fd='fdfind'
}

alias more='more -R'
alias less='less -R'

alias brew-backup='brew bundle dump'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# set PATH so it includes user's private bin if it exists
#if [ -d "$HOME/bin" ] ; then
#    PATH="$HOME/bin:$PATH"
#fi
#if [ -d "$HOME/usr/bin" ] ; then
#    PATH="$HOME/usr/bin:$PATH"
#fi
#
#if [ -d "/usr/lib/ccache" ] ; then
#    PATH="/usr/lib/ccache:$PATH"
#fi

if [ -d "$HOME/Library/Python/3.11/bin" ] ; then
  PATH="$HOME/Library/Python/3.11/bin:$PATH"
fi

if [ -d "/opt/homebrew/opt/openjdk/bin" ] ; then
  PATH="/opt/homebrew/opt/openjdk/bin:$PATH"
fi

if [ -d "/opt/homebrew/opt/llvm/bin" ] ; then
  PATH="/opt/homebrew/opt/llvm/bin:$PATH"
fi

if [ -d "/opt/homebrew/opt/ruby/bin" ] ; then
  PATH="/opt/homebrew/opt/ruby/bin:$PATH"
fi

if [ -d "/opt/homebrew/lib/ruby/gems/3.4.0/bin" ] ; then
  PATH="/opt/homebrew/lib/ruby/gems/3.4.0/bin:$PATH"
fi

export cat_cmd=cat
if [[ $OSTYPE == "darwin"* ]] ; then
  bat --version > /dev/null
  if [[  $? -eq 0 ]] ; then
    cat_cmd=bat
  fi
else
  batcat --version > /dev/null
  if [[  $? -eq 0 ]] ; then
    cat_cmd=batcat
  fi
fi
#export BAT_THEME="Monokai Extended"
alias cat=${cat_cmd}

### SET MANPAGER
### Uncomment only one of these!

### "bat" as manpager
#export MANPAGER="sh -c 'col -bx | $cat_cmd --color always -l man -p'"

### "vim" as manpager
#export MANPAGER='/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'

### "nvim" as manpager
# export MANPAGER="nvim -c 'set ft=man' -"

if [[ $OSTYPE == "darwin"* ]] ; then
  if [ ! -e "/opt/homebrew/bin/python" ] ; then
    ln -sf /opt/homebrew/bin/python3 /opt/homebrew/bin/python
  fi
fi

alias gitdiff="git difftool --dir-diff"
#alias gitdiff="git difftool --dir-diff --no-symlinks"

#alias diff='colordiff -u'
alias diff='colordiff'
alias minicom='minicom --color=on'

# disable XON/XOFF flow control
# CTRL-S: send XOFF
# CTRL-Q: send XON
stty -ixon

if [ ! -z "$TERMCAP" ] && [ "$TERM" == "screen" ] ; then
	export TERMCAP=$(echo $TERMCAP | sed -e 's/Co#8/Co#256/g')
fi

#alias ssh="ssh -X -C -c aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com"
alias ssh="ssh -C"
#	common aliases
#	ack
#alias ack="ack --nogroup --column"
alias ack="ack --nogroup --column --ignore-file=match:tags --ignore-file=match:cscope*"
alias ack-grep="ack-grep --nogroup --column --ignore-file=match:tags --ignore-file=match:cscope*"
#alias ack="ack-grep"
#sudo dpkg-divert --local --divert /usr/bin/ack --rename --add /usr/bin/ack-grep

## catppuccin_mocha
#FZF_COLOR_OPTS=" \
#  --color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
#  --color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
#  --color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"

FZF_COLOR_OPTS=" \
  --color=fg:-1,bg:-1,hl:#ff8700 \
  --color=fg+:#ffd0c0,bg+:#262626,hl+:#e05050 \
  --color=info:#50b050,prompt:#d7005f,pointer:#af5fff \
  --color=marker:#87ff00,spinner:#af5fff,header:#87afaf"

# fzf options and aliases
FZF_PREVIEW_OPTS=" --preview \
    'if [[ -f {} ]] ; then
       if [[ \$(file --mime {}) =~ binary ]] ; then
         file {}
       elif [[ \"\$cat_cmd\" == \"cat\" ]] ; then
         cat -n {}
       else
         \${cat_cmd} --style=numbers --color=always --line-range :500 {}
       fi
     elif [[ -d {} ]] ; then
       tree -C {} | less
     fi'"

#FZF_BIND_OPTS='--bind alt-j:down,alt-k:up'
FZF_DEFAULT_OPTS="--ansi \
  --layout=reverse \
  --border \
  --height 40% \
  --margin=1 \
  --padding=1 \
  --multi \
  $FZF_BIND_OPTS \
  $FZF_COLOR_OPTS \
  $FZF_PREVIEW_OPTS"

export FZF_DEFAULT_OPTS=$(echo $FZF_DEFAULT_OPTS | tr -s ' ')
# Change Directory Options
export FZF_ALT_C_OPTS="--prompt='Change Directory to > '"
# Paste the selected files and directories Options
export FZF_CTRL_T_OPTS="--prompt='Select Path > '"
export FZF_DEFAULT_COMMAND="rg --files --no-ignore-vcs --glob '!*/{.git,node_modules}/**'"
export FZF_CTRL_T_COMMAND="rg --files --no-ignore-vcs --glob '!*/{.git,node_modules}/**'"
export FZF_ALT_C_COMMAND="fd --type d --no-ignore-vcs --exclude node_modules --exclude .git"
cd_fzf() {
  dir=$1
  if [[ -z "$1" ]] ; then
    dir=.
  fi
  selected=$(find $dir -mindepth 1 ! \( -name ".*" -prune \) -type d -print | fzf)
  cd ${selected}
}

rg_fzf() {
  # 1. Search for text in files using Ripgrep
  # 2. Interactively restart Ripgrep with reload action
  # 3. Open the file in Vim
  RG_PREFIX="rg --column --line-number --no-heading --color=always --smart-case "
  INITIAL_QUERY="${*:-}"
  #IFS=: read -ra selected < <(
  #FZF_DEFAULT_COMMAND="$RG_PREFIX $(printf %q '$INITIAL_QUERY')" \
  FZF_DEFAULT_COMMAND="$RG_PREFIX '$INITIAL_QUERY'" \
    fzf --ansi \
    --disabled --query "$INITIAL_QUERY" \
    --bind "change:reload:sleep 0.1; $RG_PREFIX {q} || true" \
    --delimiter : \
    --preview '${cat_cmd} --color=always {1} --highlight-line {2}' \
    --preview-window right
  #)
  #[ -n "${selected[0]}" ] && code -g "${selected[0]}:${selected[1]}"
}

rgff() {
    rg -n --color=always $1 | fzf --ansi --no-preview
}

alias cf=cd_fzf
alias rgf=rg_fzf

alias tsd="tmux-sessionizer.sh"
alias tmux="env TERM=xterm-256color tmux"
alias ts="tmux-zoxide-sessionizer.sh"
alias tss="tmux-select-session.sh"

alias lg="lazygit"

alias repo-sync="time repo sync --force-sync -j$(nproc)"
alias repo-remote="repo forall -c \"git remote -v\""
alias repo-status="repo forall -c \"git remote -v && git status\""
alias repo-branch="repo forall -c \"git remote -v && git branch -avv\""
alias repo-clean="repo forall -c \"time git reset --hard && time git clean -fdx\" -j$(nproc)"

# docker
alias docker-m3="docker run -it --rm --init -e HOST_UID=$(id -u) -e HOST_GID=$(id -g) -v $HOME:/home/mbrain -v /tmp:/tmp -h m3-docker --name m3-docker m3"
alias docker-exec="docker exec -it"
alias docker-rmi='docker rmi $(docker images -f "dangling=true" -q)'

alias tmux-config="vim ~/.config/tmux/tmux.conf"
alias vim-config="vim ~/.vimrc"
alias vim-none="vim -Nu NONE"
alias vim-vimrc-test="vim -u ~/vimrc-test"

#	history for other terminals.
#export PROMPT_COMMAND="history -n;history -w;history -c;history -r;"
export PROMPT_COMMAND="history -a;history -c;history -r; $PROMPT_COMMAND"

WSL=`uname -a | grep -i microsoft`
echo "WSL: $WSL"
#if [ ! -z "$WSL" ] ; then
#  #export DISPLAY="localhost:0"
#  export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0
#  #export GDK_SCALE=2
#fi
unset WSL

LANG="en_US.UTF-8"
LANGUAGE="en_US:en"
export LC_ALL=$LANG

# git commit using GPG key. 2020-02-05 17:27:58
export GPG_TTY=$(tty)
``
if [[ ! -z "$IM_CONFIG_PHASE" ]] ; then
	QT_IM_MODULE=$GTK_IM_MODULE
	QT4_IM_MODULE=$GTK_IM_MODULE
	CLUTTER_IM_MODULE=$GTK_IM_MODULE
fi

export USE_CCACHE=1
export CCACHE_BASEDIR="$HOME"
export CCACHE_TEMPDIR="/tmp"
if [[ -d "/opt/nvme" ]] ; then
  export CCACHE_DIR="/opt/nvme/ccache"
  export OUT_DIR_COMMON_BASE="/opt/nvme/out"
fi

XPACK_ARM_TOOLCHAIN_PATH="$HOME/Library/xPacks/@xpack-dev-tools/arm-none-eabi-gcc"
if [[ -d "$XPACK_ARM_TOOLCHAIN_PATH" ]] ; then
  export PATH=$(find ${XPACK_ARM_TOOLCHAIN_PATH} -name bin | head -n 1):$PATH
fi

export IDF_PATH="$HOME/awair/esp-idf"
alias get_idf=". ${IDF_PATH}/export.sh"
ESP32_TOOLCHAIN_PATH="$HOME/.espressif/tools/xtensa-esp32-elf"

if [[ -d "${ESP32_TOOLCHAIN_PATH}/esp-2022r1-11.2.0" ]] ; then
  PATH="${ESP32_TOOLCHAIN_PATH}/esp-2022r1-11.2.0/xtensa-esp32-elf/bin:$PATH"
  alias esp32-addr2line="xtensa-esp32-elf-addr2line -pfiC"
elif [[ -d "${ESP32_TOOLCHAIN_PATH}/esp-2020r3-8.4.0" ]] ; then
  PATH="${ESP32_TOOLCHAIN_PATH}/esp-2020r3-8.4.0/xtensa-esp32-elf/bin:$PATH"
  alias esp32-addr2line="xtensa-esp32-elf-addr2line -pfiC"
fi

alias py-clean='find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf'

alias git-sub="git submodule update --init --recursive"
alias build-mesh-prd="time ./build.sh -p lelantus -t prd"
alias build-mesh-stg="time ./build.sh -p lelantus -t stg"
alias build-sm-prd="time ./build.sh -p surface-mount -t prd"
alias build-sm-stg="time ./build.sh -p surface-mount -t stg"

alias build-omni-prd="time make HOST_MCU_PART_NUMBER=STM32F412VG AWAIR_BUILD_TYPE=prd"
alias build-omni-stg="time make HOST_MCU_PART_NUMBER=STM32F412VG AWAIR_BUILD_TYPE=stg"
alias build-omni-512k-prd="time make HOST_MCU_PART_NUMBER=STM32F412VE AWAIR_BUILD_TYPE=prd"
alias build-omni-512k-stg="time make HOST_MCU_PART_NUMBER=STM32F412VE AWAIR_BUILD_TYPE=stg"

## Cypress FX3 ##################################################################
# Allow file access permission in MacOS
# xattr -d com.apple.quarantine ${ARMGCC_INSTALL_PATH}/bin/*
# xattr -d com.apple.quarantine ${ARMGCC_INSTALL_PATH}/arm-none-eabi/bin/*
CYPRESS_GIT_PATH="$HOME/exarion/fw-cyusb"
if [[ -d $CYPRESS_GIT_PATH ]] ; then
  cd ${CYPRESS_GIT_PATH} && source ${CYPRESS_GIT_PATH}/setenv.sh && cd - > /dev/null
fi
#################################################################################

## Podman
test -r `which podman` &> /dev/null && {
  alias docker='podman'
}

### M1 with Docker
#if [[ $OSTYPE == "darwin"* && $(uname -m) == "arm64" ]] ; then
#  export DOCKER_BUILDKIT=0 # buildkit bring issue with M1, better to disable it
#  export DOCKER_DEFAULT_PLATFORM="linux/arm64"
#fi

# enable color support of ls and also add handy aliases
if [[ $OSTYPE == "darwin"* ]] ; then
  test -r ${XDG_CONFIG_HOME}/dircolors && eval "$(gdircolors -b ${XDG_CONFIG_HOME}/dircolors)" || eval "$(gdircolors -b)"
else
  test -r ${XDG_CONFIG_HOME}/dircolors && eval "$(dircolors -b ${XDG_CONFIG_HOME}/dircolors)" || eval "$(dircolors -b)"
  #alias ls='ls --color=auto'
fi

if [[ -e "${XDG_CONFIG_HOME}/lf/lf_icons.sh" ]] ; then
  source ${XDG_CONFIG_HOME}/lf/lf_icons.sh
fi

test -r `which zoxide` &> /dev/null && {
  alias cd='z'
}
export GTK_THEME=Adwaita
echo "END $0"
